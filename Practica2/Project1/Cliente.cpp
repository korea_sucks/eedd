#include "Cliente.h"


std::string Cliente::getDNI()
{
	return dni;
}

std::string Cliente::getNombre()
{
	return nombre;
}

std::string Cliente::getPass()
{
	return pass;
}

std::string Cliente::getDireccion()
{
	return direccion;
}

Cliente::Cliente(std::string d, std::string name, std::string pasw, std::string add) :
	dni(d), nombre(name), pass(pasw), direccion(add)
{

}

Cliente::~Cliente()
{
}
