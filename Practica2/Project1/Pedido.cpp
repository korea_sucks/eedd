#include "Pedido.h"


Pedido::Pedido(Cliente & cli) : user(cli), estado("RECIBIDO")
{
	id = cli.getDNI();
	cesta = new VDinamico<Producto>;
}

Cliente & Pedido::getCliente()
{
	return user;
}

std::string Pedido::getEstado()
{
	return estado;
}

std::string Pedido::getID()
{
	return id;
}

void Pedido::nuevoProducto(Producto& prod)
{
	cesta->insertar(prod);
}

float Pedido::importe()
{
	float suma = 0;
	int t = cesta->tam();
	for (int i = 0; i < t; i++)
	{
		suma += cesta->operator[](i).getPvp();
	}
	return suma;
}

Pedido::~Pedido()
{
	delete cesta;
}

void Pedido::actualizarEstado(std::string update)
{
	if (update != "PAGADO" || update != "ENALMACEN" || update != "ENTRASITO" || update != "ENTREGADO")
		throw std::invalid_argument("Estado incorrecto");
	else
		estado = update;
}
