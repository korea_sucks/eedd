#pragma once
#include "Cliente.h"
#include "Producto.h"
#include "ListaEnlazada.h"
#include "vdinamico.h"

class Pedido
{
private:
	std::string id;
	std::string estado;
	Cliente user;
public:
	VDinamico<Producto>* cesta;
	Pedido(Cliente& cli);
	Cliente& getCliente();
	std::string getEstado();
	std::string getID();
	void nuevoProducto(Producto& prod);
	float importe();
	~Pedido();
	void actualizarEstado(std::string update);
};

