#include <iostream>
#include <cmath>
#include"vdinamico.h"
#include"Producto.h"
#include"Nodo.h"
#include"ListaEnlazada.h"
#include"Iterador.h"
#include"Cliente.h"
#include"Pedido.h"

template <class T>
void leerProducto(VDinamico<T>& v, std::string nomArchivo) {
	std::cout << "Leyendo fichero \n";
	std::ifstream fichero;
	std::string linea;
	fichero.open(nomArchivo.c_str());
	if (fichero.good()) {

		bool fin = false;
		while (fin == false) {
			T aux;
			getline(fichero, linea);
			if (fichero.eof()) {
				fin = true;
			}
			else {
				aux.fromCSV(linea);
				v.insertar(aux);
			}
			
		}
		fichero.close();
		std::cout << "Fichero leido \n";
	}
	else
	{
		throw std::invalid_argument("El fichero no se puede abrir o est� danado");
	}
}

template <class T>
void leerProducto(ListaEnlazada<T>& l, std::string nomArchivo) {
	std::cout << "Leyendo fichero \n";
	std::ifstream fichero;
	std::string linea;
	fichero.open(nomArchivo.c_str());
	if (fichero.good()) {

		bool fin = false;
		while (fin == false) {
			T aux;
			getline(fichero, linea);
			if (fichero.eof()) {
				fin = true;
			}
			else {
				aux.fromCSV(linea);
				l.insertaFinal(aux);
			}

		}
		fichero.close();
		std::cout << "Fichero leido \n======================================================\n";
	}
	else
	{
		throw std::invalid_argument("El fichero no se puede abrir o est� danado");
	}
}

void visualiza(Pedido& ped)
{
	std::cout << "\nPedido: " << ped.getID() << "\nDirecion de entrega: " << ped.getCliente().getDireccion() << "\nNombre: " << ped.getCliente().getNombre() << "\nDNI: " << ped.getCliente().getDNI() << "\n";
	std::cout << "======================================================\nCesta: \n";
	int t = ped.cesta->tam();
	for (int i = 0; i < t; i++)
	{
		std::cout << ped.cesta->operator[](i).getDesc() << ", " << ped.cesta->operator[](i).getPvp() << "EUR\n";
	}
	std::cout << "======================================================\nTotal: " << ped.importe() << "EUR\n\n";
};

int main()
{
	try
	{
		ListaEnlazada<Producto> lista;
		ListaEnlazada<Producto *> cremas, aguas;
		leerProducto(lista, "Pharma3.csv");
		Iterador<Producto> i = lista.iterador();
		while (!i.fin())
		{
			if (i.editarDato().getDesc().find("Crema") != -1 || i.editarDato().getDesc().find("CREMA") != -1)
			{
				Producto* aux = &i.editarDato();
				cremas.insertaFinal(aux);
			}
			if (i.editarDato().getDesc().find("AGUA") != -1 || i.editarDato().getDesc().find("agua") != -1)
			{
				Producto* aux = &i.editarDato();
				aguas.insertaFinal(aux);
			}
			i.siguiente();
		}
		ListaEnlazada<Producto *> concatenada = cremas.concatena(aguas);
		Iterador<Producto *> i2 = concatenada.iterador();
		while (!i2.fin())
		{
			std::cout << i2.editarDato()->getDesc() << "\n";
			i2.siguiente();
		}
		std::cout << "======================================================\n";
		system("Pause");
		i = lista.iterador();
		Cliente Corea;
		Pedido test1(Corea);
		int counter = 21;
		while (!counter == 0)
		{
			if (i.editarDato().getDesc().find("100ML") != -1 || i.editarDato().getDesc().find("100ml") != -1)
			{
				Producto aux = i.editarDato();
				test1.nuevoProducto(aux);
				counter--;
			}
			i.siguiente();
		}
		visualiza(test1);
		system("Pause");

		return 0;
	}
	catch (std::exception &e)
	{
		std::cerr << e.what() << "\n";
	}
}