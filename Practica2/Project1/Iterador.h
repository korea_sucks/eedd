#pragma once
#include "Nodo.h"

template<class T>
class Iterador
{
public:
	void siguiente();
	bool fin();
	T& editarDato();
	Iterador(Nodo<T>* cabeza);
	~Iterador();

private:
	Nodo<T>* current;
	template <class T> friend class ListaEnlazada;
};

template<class T>
void Iterador<T>::siguiente()
{
	if (fin() == true)
	{
		
	}
	else
		current = current->sig;
}

template<class T>
bool Iterador<T>::fin()
{
	if (current->sig == 0)
		return true;
	else
	return false;
}

template<class T>
T& Iterador<T>::editarDato()
{
	return current->dato;
}

template<class T>
Iterador<T>::Iterador(Nodo<T> * cabeza) : current(cabeza)
{

}

template<class T>
Iterador<T>::~Iterador()
{

}
