#include "PharmaDron.h"



PharmaDron::PharmaDron()
{
}

void PharmaDron::cargaProductos(std::string fileNameProductos)
{
	std::cout << "Leyendo fichero \n";
	std::ifstream fichero;
	std::string linea;
	fichero.open(fileNameProductos.c_str());
	if (fichero.good()) {

		bool fin = false;
		while (fin == false) {
			Producto aux;
			getline(fichero, linea);
			if (fichero.eof()) {
				fin = true;
			}
			else {
				aux.fromCSV(linea);
				productos.insertaFinal(aux);
			}

		}
		fichero.close();
		std::cout << "Fichero leido \n======================================================\n";
	}
	else
	{
		throw std::invalid_argument("El fichero no se puede abrir o est� danado");
	}
}

void PharmaDron::creaClientes(std::string fileNameClientes)
{
	std::cout << "Leyendo fichero \n";
	std::ifstream fichero;
	std::string linea;
	fichero.open(fileNameClientes.c_str());
	if (fichero.good()) {

		bool fin = false;
		while (fin == false) {
			Cliente aux(this);
			getline(fichero, linea);
			if (fichero.eof()) {
				fin = true;
			}
			else {
				aux.fromCSV(linea);
				clientes.insertar(aux);
			}

		}
		fichero.close();
		std::cout << "Fichero leido \n======================================================\n";
	}
	else
	{
		throw std::invalid_argument("El fichero no se puede abrir o est� danado");
	}
}

void PharmaDron::aniadeProducto(Producto p)
{
	productos.insertaFinal(p);
}

void PharmaDron::nuevoCliente(Cliente c)
{
	clientes.insertar(c);
}

ListaEnlazada<Producto*> PharmaDron::buscaProducto(std::string subcadena)
{
	ListaEnlazada<Producto*> busqueda;
	Iterador<Producto> i = productos.iterador();
	while (!i.fin())
	{
		if (i.editarDato().getDesc().find(subcadena) != -1)
		{
			Producto *aux = &i.editarDato();
			busqueda.insertaFinal(aux);
		}
		i.siguiente();
	}
	return busqueda;
}

Cliente PharmaDron::ingresaCliente(std::string dni, std::string pass)
{
	Cliente aux(dni);
	Cliente result;
	bool esta = clientes.busca(aux, result);
	if (esta)
	{
		if (pass == result.getPass())
			return result;
		if (pass != result.getPass())
			throw std::invalid_argument("Contrase�a incorrecta");
	}
	else
		throw std::invalid_argument("El Cliente no esta registrado");
}


Pedido PharmaDron::verPedido(Cliente c)
{
	return c.getPedido();
}

PharmaDron::~PharmaDron()
{
}
