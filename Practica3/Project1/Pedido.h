#pragma once
#include "Producto.h"
#include "ListaEnlazada.h"
#include "vdinamico.h"


class Pedido
{
private:
	std::string id;
	std::string estado;
public:
	VDinamico<Producto>* cesta;
	Pedido(const Pedido& orig);
	Pedido(std::string idd = "");
	//Pedido(Cliente& cli);
	void operator=(Pedido& orig);
	//Cliente& getCliente();
	std::string getEstado();
	std::string getID();
	void nuevoProducto(Producto& prod);
	float importe();
	~Pedido();
	void actualizarEstado(std::string update);
};

