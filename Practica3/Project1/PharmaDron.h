#pragma once
#include "ListaEnlazada.h"
#include "Producto.h"
#include"Iterador.h"
#include"Cliente.h"
#include"Pedido.h"
#include"ArbolAVL.h"

class PharmaDron
{
private:
	ListaEnlazada<Producto> productos;
	AVL<Cliente> clientes;
public:
	PharmaDron();
	void cargaProductos(std::string fileNameProductos);
	void creaClientes(std::string fileNameClientes);
	void aniadeProducto(Producto p);
	void nuevoCliente(Cliente c);
	ListaEnlazada<Producto*> buscaProducto(std::string subcadena);
	Cliente ingresaCliente(std::string dni, std::string pass);
	Pedido verPedido(Cliente c);
	virtual ~PharmaDron();
};

