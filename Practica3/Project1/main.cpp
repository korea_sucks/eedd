#pragma once
#include <iostream>
#include <cmath>
#include"vdinamico.h"
#include"Producto.h"
#include"Nodo.h"
#include"ListaEnlazada.h"
#include"Iterador.h"
#include"PharmaDron.h"
#include"Cliente.h"
#include"Pedido.h"
#include"ArbolAVL.h"

template <class T>
void leerProducto(VDinamico<T>& v, std::string nomArchivo) {
	std::cout << "Leyendo fichero \n";
	std::ifstream fichero;
	std::string linea;
	fichero.open(nomArchivo.c_str());
	if (fichero.good()) {

		bool fin = false;
		while (fin == false) {
			T aux;
			getline(fichero, linea);
			if (fichero.eof()) {
				fin = true;
			}
			else {
				aux.fromCSV(linea);
				v.insertar(aux);
			}
			
		}
		fichero.close();
		std::cout << "Fichero leido \n";
	}
	else
	{
		throw std::invalid_argument("El fichero no se puede abrir o est� danado");
	}
}

template <class T>
void leerProducto(ListaEnlazada<T>& l, std::string nomArchivo) {
	std::cout << "Leyendo fichero \n";
	std::ifstream fichero;
	std::string linea;
	fichero.open(nomArchivo.c_str());
	if (fichero.good()) {

		bool fin = false;
		while (fin == false) {
			T aux;
			getline(fichero, linea);
			if (fichero.eof()) {
				fin = true;
			}
			else {
				aux.fromCSV(linea);
				l.insertaFinal(aux);
			}

		}
		fichero.close();
		std::cout << "Fichero leido \n======================================================\n";
	}
	else
	{
		throw std::invalid_argument("El fichero no se puede abrir o est� danado");
	}
}


void visualiza(Cliente& ped)
{
	std::cout << "\nPedido: " << ped.getPedido().getID() << "\nDirecion de entrega: " << ped.getDireccion() << "\nNombre: " << ped.getNombre() << "\nDNI: " << ped.getDNI() << "\n";
	std::cout << "======================================================\nCesta: \n";
	int t = ped.getPedido().cesta->tam();
	for (int i = 0; i < t; i++)
	{
		std::cout << ped.getPedido().cesta->operator[](i).getDesc() << ", " << ped.getPedido().cesta->operator[](i).getPvp() << "EUR\n";
	}
	std::cout << "======================================================\nTotal: " << ped.getPedido().importe() << "EUR\n\n";
};

int main()
{
	//Prueba 1
	
	AVL<int> arbol;
	for (int i = 0; i < 1000000; i++) 
	{
		arbol.insertar(i);
	}
	std::cout << "Altura del arbol: " << arbol.altura() << "\n";
	for (int i = 0; i < 2; i++)
	{
		int j = 0;
		int k = 0;
		std::cout << "Numero a buscar: ";
		std::cin >> j;
		bool esta = arbol.busca(j,k);
		if (esta)
			std::cout << "Busqueda Por Clave Encontrado\n";
		else
			std::cout << "Busqueda Por Clave No Encontrado\n";
		esta = arbol.buscaIt(j, k);
		if (esta)
			std::cout << "Busqueda Iterativa Encontrado\n";
		else
			std::cout << "Busqueda Iterativa No Encontrado\n";
	}
	system("pause");
	
	//Prueba 2
	
	try
	{
		PharmaDron pharma;
		pharma.cargaProductos("pharma3.csv");
		pharma.creaClientes("clientes.csv");
		system("pause");

		std::cout << "Iniciando busquedad de clientes(DNI)\n======================================================\nCliente 1: ";
		std::string a, b, c, ap, bp, cp;
		
		std::cin >> a;
		std::cout << "\nCliente 1 (contrase�a): ";
		std::cin >> ap;
		Cliente a1(pharma.ingresaCliente(a, ap));

		std::cout << "\nCliente 2: ";
		std::cin >> b;
		std::cout << "\nCliente 2 (contrase�a): ";
		std::cin >> bp;
		Cliente b2(pharma.ingresaCliente(b, bp));
		
		std::cout << "\nCliente 3: ";
		std::cin >> c;
		std::cout << "\nCliente 3 (contrase�a): ";
		std::cin >> cp;
		Cliente c3(pharma.ingresaCliente(c, cp));

		std::cout << "======================================================\nClientes Leidos" << "\n";
		system("pause");
		std::cout << "Preparando Pedidos\n";

		ListaEnlazada<Producto*> al, bl, al1, al2, bl1, bl2;
		al1 = pharma.buscaProducto("100ml");
		al2 = pharma.buscaProducto("100ML");
		al = al1.concatena(al2);
		
		bl1 = pharma.buscaProducto("Crema");
		bl2 = pharma.buscaProducto("CREMA");
		bl = bl1.concatena(bl2);
		
		std::cout << "======================================================\nPedidos Preparados" << "\n";
		system("pause");
		std::cout << "Cargando Pedidos\n";
		std::cout << "======================================================\nPrimer Cliente:\n";

		Iterador<Producto*> it = al.iterador();
		while(!(it.fin()))
		{
			Producto* aux = it.editarDato();
			a1.addProducto(*aux);
			it.siguiente();
		}

		visualiza(a1);
		std::cout << "\n======================================================\n";
		system("pause");
		
		it = bl.iterador();
		while (!(it.fin()))
		{
			Producto* aux = it.editarDato();
			b2.addProducto(*aux);
			it.siguiente();
		}

		visualiza(b2);
		std::cout << "\n======================================================\n";
		system("pause");
		
		it = al.iterador();
		while (!(it.fin()))
		{
			Producto* aux = it.editarDato();
			c3.addProducto(*aux);
			it.siguiente();
		}
		it = bl.iterador();
		while (!(it.fin()))
		{
			if (it.editarDato()->getDesc().find("100ml") == -1 && it.editarDato()->getDesc().find("100ML") == -1)
			{
				Producto* aux = it.editarDato();
				c3.addProducto(*aux);
			}
			it.siguiente();
		}

		visualiza(c3);
		std::cout << "\n======================================================\nPedidos Cargados\n";
		
		system("pause");
	}
	catch (const std::exception& e)
	{
		std::cerr << e.what() << "\n";
		system("pause");
	}
	
	/*
	try
	{
		ListaEnlazada<Producto> lista;
		ListaEnlazada<Producto *> cremas, aguas;
		leerProducto(lista, "Pharma3.csv");
		Iterador<Producto> i = lista.iterador();
		while (!i.fin())
		{
			if (i.editarDato().getDesc().find("Crema") != -1 || i.editarDato().getDesc().find("CREMA") != -1)
			{
				Producto* aux = &i.editarDato();
				cremas.insertaFinal(aux);
			}
			if (i.editarDato().getDesc().find("AGUA") != -1 || i.editarDato().getDesc().find("agua") != -1)
			{
				Producto* aux = &i.editarDato();
				aguas.insertaFinal(aux);
			}
			i.siguiente();
		}
		ListaEnlazada<Producto *> concatenada = cremas.concatena(aguas);
		Iterador<Producto *> i2 = concatenada.iterador();
		while (!i2.fin())
		{
			std::cout << i2.editarDato()->getDesc() << "\n";
			i2.siguiente();
		}
		std::cout << "======================================================\n";
		system("Pause");
		i = lista.iterador();
		Cliente Corea;
		Pedido test1(Corea);
		int counter = 21;
		while (!counter == 0)
		{
			if (i.editarDato().getDesc().find("100ML") != -1 || i.editarDato().getDesc().find("100ml") != -1)
			{
				Producto aux = i.editarDato();
				test1.nuevoProducto(aux);
				counter--;
			}
			i.siguiente();
		}
		visualiza(test1);
		system("Pause");

		return 0;
	}
	catch (std::exception &e)
	{
		std::cerr << e.what() << "\n";
	}
	*/
}