#pragma once
#include "Producto.h"
#include "Cliente.h"
#include "Pedido.h"
#include "THashCerradaProducto.h"
#include <vector>
#include <list>
#include <map>
#include "Dron.h"
#include "MallaRegular.h"


class PharmaDron
{
private:
	THashCerradaProducto produ;
	std::list<Producto> productos;
	std::map<std::string, Cliente> clientes;
	std::vector<Dron*> drones;
	MallaRegular<Cliente*> AccesoUTM;
public:
	PharmaDron(int tamTabla, float aXMin, float aYMin, float aXMax, float aYMax, int nDivX, int nDivY);
	void aniadeDron(Dron * neuvo);
	void cargaProductos(std::string fileNameProductos);
	void creaClientes(std::string fileNameClientes);
	void aniadeProducto(Producto p);
	void nuevoCliente(Cliente c);
	void borraCliente(std::string subcadena);
	unsigned int getCargaX();
	unsigned int getCargaY();
	unsigned int clientesCelda();
	void reajustarMalla(int nDivX, int nDivY);
	std::vector<Cliente*> buscaClienteCuad(UTM pos, float tama);
	Dron* dronDisponible();
	std::list<Producto*> buscaProducto(std::string subcadena);
	Cliente* ingresaCliente(std::string dni, std::string pass);
	Pedido* verPedido(Cliente c);
	std::vector<Producto*> buscaTermino(std::string termino);
	int llevarPedidosZona(Dron* dron ,UTM pos, float tama);
	virtual ~PharmaDron();
};

