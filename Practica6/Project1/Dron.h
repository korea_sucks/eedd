#pragma once
#include "UTM.h"
#include "Cliente.h"
#include <vector>

class Dron
{
private:
	static int drones;
	int id;
	UTM pos;
	bool disponible;
	std::vector<Cliente*> clientes;
public:
	void aniadeCliente(Cliente* c);
	int getId();
	void ocupar();
	bool getEstado();
	UTM* getPos();
	int entregarPedidos();
	Dron(UTM posi);
	~Dron();
};