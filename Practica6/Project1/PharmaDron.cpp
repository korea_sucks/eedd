#include "PharmaDron.h"

PharmaDron::PharmaDron(int tamPharma, float aXMin, float aYMin, float aXMax, float aYMax, int nDivX, int nDivY) :
	produ(THashCerradaProducto(tamPharma)), AccesoUTM(MallaRegular<Cliente*>(aXMin, aYMin, aXMax, aYMax, nDivX, nDivY))
{
}

void PharmaDron::aniadeDron(Dron * neuvo)
{
	drones.push_back(neuvo);
}

void PharmaDron::cargaProductos(std::string fileNameProductos)
{
	std::cout << "Leyendo fichero \n";
	std::ifstream fichero;
	std::string linea;
	fichero.open(fileNameProductos.c_str());
	if (fichero.good()) {

		bool fin = false;
		while (fin == false) {
			Producto aux;
			getline(fichero, linea);
			if (fichero.eof()) {
				fin = true;
			}
			else {
				aux.fromCSV(linea);
				productos.insert(productos.end(),aux);
				std::list<Producto>::iterator ite = productos.end();
				ite--;
				produ.metedatos(&ite._Ptr->_Myval);
			}

		}
		fichero.close();
		std::cout << "Fichero leido \n======================================================\n";
	}
	else
	{
		throw std::invalid_argument("El fichero no se puede abrir o est� danado");
	}
}

void PharmaDron::creaClientes(std::string fileNameClientes)
{
	std::cout << "Leyendo fichero \n";
	std::ifstream fichero;
	std::string linea;
	fichero.open(fileNameClientes.c_str());
	if (fichero.good()) {

		bool fin = false;
		while (fin == false) {
			Cliente aux(this);
			getline(fichero, linea);
			if (fichero.eof()) {
				fin = true;
			}
			else {
				aux.fromCSV(linea);
				clientes.insert(std::pair<std::string, Cliente>(aux.getDNI(),aux));
			}

		}
		for (std::map<std::string, Cliente>::iterator it = clientes.begin(); it != clientes.end(); it++)
		{
			AccesoUTM.insertar(it._Ptr->_Myval.second.getX(), it._Ptr->_Myval.second.getY(), &it._Ptr->_Myval.second);
		}
		fichero.close();
		std::cout << "Fichero leido \n======================================================\n";
	}
	else
	{
		throw std::invalid_argument("El fichero no se puede abrir o est� danado");
	}
}

void PharmaDron::aniadeProducto(Producto p)
{
	productos.insert(productos.end(),p);
}

void PharmaDron::nuevoCliente(Cliente c)
{
	clientes.insert(std::pair<std::string, Cliente>(c.getDNI() ,c));
}

void PharmaDron::borraCliente(std::string subcadena)
{
	std::map<std::string, Cliente>::iterator aux;
	aux = clientes.find(subcadena);
	if(aux._Ptr != 0)
		clientes.erase(aux);
}

unsigned int PharmaDron::getCargaX()
{
	return AccesoUTM.maxElementosPorFila();
}

unsigned int PharmaDron::getCargaY()
{
	return AccesoUTM.maxElementosporColumna();
}

unsigned int PharmaDron::clientesCelda()
{
	return AccesoUTM.maxElementosPorCelda();
}

void PharmaDron::reajustarMalla(int nDivX, int nDivY)
{
	AccesoUTM.reajustar(nDivX, nDivY);
}

std::vector<Cliente*> PharmaDron::buscaClienteCuad(UTM pos, float tama)
{
	return AccesoUTM.buscarRango(pos.getLatitud() - tama, pos.getLongitud() - tama, pos.getLatitud() + tama, pos.getLongitud() + tama);

}

Dron * PharmaDron::dronDisponible()
{
	Dron* aux = nullptr;
	int i = 0;
	while (aux == nullptr && i < drones.size())
	{
		if (drones[i]->getEstado() == true)
			aux = drones[i];
		i++;
	}
	return aux;
}

std::list<Producto*> PharmaDron::buscaProducto(std::string subcadena)
{
	std::list<Producto*> busqueda;
	std::list<Producto>::iterator i = productos.begin();
	while (i != productos.end())
	{
		if (i._Ptr->_Myval.getDesc().find(subcadena) != -1)
		{
			Producto *aux = &i._Ptr->_Myval;
			busqueda.insert(busqueda.end(), aux);
		}
		i++;
	}
	return busqueda;
}

Cliente* PharmaDron::ingresaCliente(std::string dni, std::string pass)
{
	Cliente aux(dni);
	Cliente result;
	std::map<std::string, Cliente>::iterator it = clientes.find(dni);
	if (it != clientes.end())
	{
		if (pass == it._Ptr->_Myval.second.getPass())
			return &it._Ptr->_Myval.second;
		if (pass != it._Ptr->_Myval.second.getPass())
			throw std::invalid_argument("Contrase�a incorrecta");
	}
	else
		throw std::invalid_argument("El Cliente no esta registrado");
}


Pedido* PharmaDron::verPedido(Cliente c)
{
	return c.getPedido();
}

std::vector<Producto*> PharmaDron::buscaTermino(std::string termino)
{
	return produ.buscar((djb2((unsigned char*)termino.c_str())), termino);
}

int PharmaDron::llevarPedidosZona(Dron* dron, UTM pos, float tama)
{
	dron->ocupar();
	std::vector<Cliente*> cli = buscaClienteCuad(pos, tama);
	for (int i = 0; i < cli.size(); i++)
	{
		dron->aniadeCliente(cli[i]);
	}
	return dron->entregarPedidos();
}

PharmaDron::~PharmaDron()
{
}
