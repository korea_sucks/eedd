#pragma once
#include "Producto.h"
#include <vector>

class Pedido
{
private:
	std::string id;
	std::string estado;
	std::vector<Producto*> cesta;
public:
	Pedido(const Pedido& orig);
	std::vector<Producto*>* getCesta();
	Pedido(std::string idd = "");
	//Pedido(Cliente& cli);
	void operator=(Pedido& orig);
	//Cliente& getCliente();
	std::string getEstado();
	std::string getID();
	void nuevoProducto(Producto* prod);
	float importe();
	~Pedido();
	void actualizarEstado(std::string update);
};

