#include "Dron.h"

int Dron::drones = 1;

void Dron::aniadeCliente(Cliente * c)
{
	clientes.push_back(c);
}

int Dron::getId()
{
	return id;
}

void Dron::ocupar()
{
	disponible = false;
}

bool Dron::getEstado()
{
	return disponible;
}

UTM * Dron::getPos()
{
	return &pos;
}

int Dron::entregarPedidos()
{
	int retorno = 0;
	if (clientes.size() != 0)
	{
		for (int i = 0; i < clientes.size(); i++)
		{
			clientes[i]->getPedido()->actualizarEstado("ENTREGADO");
			retorno++;
		}
	}
	else
	{
		std::cout << "Dron: No tengo clientes asignados D: \n";
	}
	//disponible = true;
	return retorno;
}

Dron::Dron(UTM posi) : disponible(true), id(drones), pos(posi)
{
	drones++;
}


Dron::~Dron()
{

}