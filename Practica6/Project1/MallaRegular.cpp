/*
#include <list>

template<typename T>
void Casilla<T>::insertar(const T & dato)
{
	puntos.push_back(dato);
}

template<class T>
T * Casilla<T>::buscar(const T & dato)
{
	typename list <T>::iterator it;
	it = puntos.begin();
	for (; it != puntos.end(); ++it)
	{
		if (*it == dato)
			return &(*it);
	}
	return 0;
}

template<typename T>
bool Casilla<T>::borrar(const T & dato)
{
	typename list<T>::iterator it;
	it = puntos.begin();
	for (; it != puntos.end(); ++it)
	{
		if (*it == dato)
		{
			puntos.erase(it);
			return true;
		}
	}
	return false;
}

template<typename T>
std::list<T> Casilla<T>::getLista()
{
	return puntos;
}

template<typename T>
void Casilla<T>::setLista(std::list<T>& p)
{
	puntos = p;
}

template <typename T>
Casilla<T>::~Casilla()
{
}

template<typename T>
Casilla<T>* MallaRegular<T>::obtenerCasilla(float x, float y)
{
	int i = (x - xMin) / tamaCasillaX;
	int j = (y - yMin) / tamaCasillaY;
	return &mr[j][i];
}

template<typename T>
MallaRegular<T>::MallaRegular(float aXMin, float aYMin, float aXMax, float aYMax, int nDivX, int nDivY):
		xMin(aXMin),
		xMax(aXMax),
		yMin(aYMin),
		yMax(aYMax)
{
	tamaCasillaX = (xMax - xMin) / nDivX;
	tamaCasillaY = (yMax - yMin) / nDivY;
	mr.insert(mr.begin(), nDivY, std::vector<Casilla<T>>(nDivX));
}

template<typename T>
void MallaRegular<T>::reajustar(int nDivX, int nDivY)
{
	std::vector<std::vector<Casilla<T>>> aux;
	aux.insert(aux.begin(), nDivY, std::vector<Casilla<T>>(nDivX));
	std::list<T> backup;
	float NtCX = (xMax - xMin) / nDivX;
	float NtCY = (yMax - yMin) / nDivY;
	float RtX = NtCX / tamaCasillaX;
	float RtY = NtCY / tamaCasillaY;
	for (float i = 0; i < ((xMax - xMin)/tamaCasillaX); i++)
	{
		for (float j = 0; j < ((yMax - yMin) / tamaCasillaY); j++)
		{
			backup=mr[j][i].getLista();
			for (int k = 0; k < backup.size(); k++)
			{
				aux[(j / RtY)][(i / RtX)].setLista(backup);
			}
		}
	}
	tamaCasillaX = NtCX;
	tamaCasillaY = NtCY;
	mr = aux;
}

template<typename T>
std::vector<T> MallaRegular<T>::buscarRango(float rxmin, float rymin, float rxmax, float rymax)
{
	std::vector<T> puntosR;
	int iMin = (rxmin - xMin) / tamaCasillaX;
	int jMin = (rymin - yMin) / tamaCasillaY;
	int iMax = (rxmax - xMin) / tamaCasillaX;
	int jMax = (rymax - yMin) / tamaCasillaY;
	for (iMin; iMin < iMax; iMin++)
	{
		for (jMin; jMin < jMax; jMin++)
		{
			std::list<T> aux = mr[jMin][iMin].getLista();
			typename std::list<T>::iterator ite = aux.begin();
			for (int i = 0 ; i < aux.size(); i++)
			{
				puntosR.push_back(*ite);
				ite++;
			}
		}
	}
	return puntosR;
}

template<typename T>
void MallaRegular<T>::insertar(float x, float y, const T & dato)
{
	Casilla<T>* c = obtenerCasilla(x, y);
	c->insertar(dato);
}

template<class T>
unsigned int MallaRegular<T>::maxElementosPorCelda()
{
	unsigned int x = 0;
	for (int i = 0; i < yMax; i++)
	{
		for (int j = 0; j < xMax; j++)
		{
			if (mr[i][j].puntos.size() > x)
				x = mr[i][j].puntos.size();
		}
	}
	return x;
}

template<class T>
unsigned int MallaRegular<T>::maxElementosPorFila()
{
	unsigned int x = 0;
	for (int i = 0; i < yMax; i++)
	{
		unsigned int fila = 0;
		for (int j = 0; j < xMax; j++)
		{
			if (mr[i][j].puntos.size() > x)
				fila += mr[i][j].puntos.size();
		}
		if (fila > x)
			x = fila;
	}
	return x;
}

template<class T>
unsigned int MallaRegular<T>::maxElementosporColumna()
{
	unsigned int x = 0;
	for (int i = 0; i < xMax; i++)
	{
		unsigned int columna = 0;
		for (int j = 0; j < yMax; j++)
		{
			if (mr[i][j].puntos.size() > x)
				columna += mr[i][j].puntos.size();
		}
		if (columna > x)
			x = columna;
	}
	return x;
}

template<class T>
float MallaRegular<T>::promedioElementosPorCelda()
{
	float sum = 0.0f;
	for (int i = 0; i < yMax; i++)
	{
		for (int j = 0; j < xMax; j++)
		{
			sum += mr[i][j].puntos.size();
		}
	}
	return sum / (tamaCasillaX * tamaCasillaX);
}

template <typename T>
MallaRegular<T>::~MallaRegular()
{
}
*/