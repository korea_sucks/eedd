#pragma once
#include <iostream>

template <typename U>
class Hoja {
public:
	Hoja<U> *izq, *der;
	U dato;
	char bal;

	Hoja(U &ele) : izq(nullptr), der(nullptr), bal(0), dato(ele) {}
	Hoja<U>(Hoja<U>* iz, Hoja<U>* de, char ba, U& dato) : izq(iz), der(de), bal(ba), dato(dato) {}
};
template <typename T>
class AVL {
private:
	int inserta(Hoja<T>* &c, T &dato);
	void rotDecha(Hoja<T>* &p);
	void rotIzqda(Hoja<T>* &p);
	unsigned int num;
	void inorden(Hoja<T> *p, unsigned int nivel);
	Hoja<T>* buscaClave(T &ele, Hoja<T> *p);
	Hoja<T>* Iteracion(Hoja<T> *p);
	Hoja<T>* raiz;

	void destruye(Hoja<T>*p);
	void buscaItaux(Hoja<T> *p, T& dato, bool& result, T& re);
	void preordenC(Hoja<T>* p, Hoja<T>*& o);
	void inordenAlt(Hoja<T>* p, unsigned int nivel, unsigned int& altura);

public:
	AVL<T>();
	bool insertar(T &dato);
	AVL<T>(const AVL<T> &orig);
	AVL<T>& operator= (AVL<T> &orig);
	bool busca(T& dato, T& result);
	bool buscaIt(T& dato, T& result);
	void recorreInorden();
	unsigned int numElementos();
	unsigned int altura();
	~AVL<T>();
};

template <typename T>
AVL<T>::AVL():
	num(0),
	raiz(0)
{
}

template<typename T>
AVL<T>::AVL(const AVL<T>& orig):
	num(orig.num)
{
	Hoja<T> *original = orig.raiz;
	preordenC(original, raiz);
}

template<typename T>
void AVL<T>::preordenC(Hoja<T>* p, Hoja<T>*& o)
{
	if (p)
	{
		Hoja<T>* aux = new Hoja<T>(0, 0, p->bal, p->dato);
		o = aux;
		preordenC(p->izq, o->izq);
		preordenC(p->der, o->der);
	}
}

template<typename T>
AVL<T>::~AVL()
{
	/*
	Hoja<T>* aux = raiz;
	while (raiz != nullptr)
	{
		while (Iteracion(aux) != nullptr)
			aux = Iteracion(aux);
		delete aux;
		aux = raiz;
	}
	*/
	destruye(raiz);
}

template <typename T>
int AVL<T>::inserta(Hoja<T>*& c, T & dato)
{
	Hoja<T> *p = c;
	int deltaH = 0;
	if (!p)
	{
		p = new Hoja<T>(dato);
		c = p;
		deltaH = 1;
	}
	else if (dato > p->dato)
	{
		if (inserta(p->der, dato))
		{
			p->bal--;
			if (p->bal == -1)
				deltaH = 1;
			else if (p->bal == -2)
			{
				if (p->der->bal == 1)
					rotDecha(p->der);
				rotIzqda(c);
			}
		}
	}
	else if (dato < p->dato)
	{
		if (inserta(p->izq, dato))
		{
			p->bal++;
			if (p->bal == 1)
				deltaH = 1;
			else if (p->bal == 2)
			{
				if (p->izq->bal == -1)
					rotIzqda(p->izq);
				rotDecha(c);
			}
		}
	}
	return deltaH;
}

template <typename T>
void AVL<T>::rotDecha(Hoja<T>*& p)
{
	Hoja<T> *q = p, *l;
	p = l = q->izq;
	q->izq = l->der;
	l->der = q;
	q->bal--;
	if (l->bal > 0)
		q->bal -= l->bal;
	l->bal--;
	if (q->bal < 0)
		l->bal -= -q->bal;
}

template <typename T>
void AVL<T>::rotIzqda(Hoja<T>*& p)
{
	Hoja<T> *q = p, *r;
	p = r = q->der;
	q->der = r->izq;
	r->izq = q;
	q->bal++;
	if (r->bal < 0)
		q->bal += -r->bal;
	r->bal++;
	if (q->bal > 0)
		r->bal += q->bal;
}

template<typename T>
void AVL<T>::inorden(Hoja<T>* p, unsigned int nivel)
{
	if (p)
	{
		inorden(p->izq, nivel + 1);
		std::cout << "Procesando nodo " << p->dato;
		std::cout << " en el nivel " << nivel << "\n";
		inorden(p->der, nivel + 1);
	}
}

template<typename T>
Hoja<T>* AVL<T>::buscaClave(T & ele, Hoja<T>* p)
{
	if (!p)
		return 0;
	else if (ele < p->dato)
		return buscaClave(ele, p->izq);
	else if(ele > p->dato)
		return buscaClave(ele, p->der);
	else return p;
}

template<typename T>
Hoja<T>* AVL<T>::Iteracion(Hoja<T>* p)
{
	if (p)
	{
		if (p->izq == nullptr)
			return p->der;
		else return p->izq;
	}
	return nullptr;
}

template<typename T>
void AVL<T>::destruye(Hoja<T>* p)
{
	if (p != 0)
	{
		destruye(p->izq);
		destruye(p->der);
		delete p;
		p = 0;
	}
}

template<typename T>
void AVL<T>::buscaItaux(Hoja<T>* p, T & dato, bool & result, T& re)
{
	if (p)
	{
		buscaItaux(p->izq, dato, result,re);
		if (p->dato == dato)
		{
			result = true;
			re = p->dato;
		}
		buscaItaux(p->der, dato, result,re);
	}
}

template <typename T>
bool AVL<T>::insertar(T& dato)
{
	T resutlado;
	bool encontrado = busca(dato, resutlado);
	if (!encontrado)
	{
		inserta(raiz, dato);
		num++;
		return true;
	}
	return false;
}

template<typename T>
AVL<T>& AVL<T>::operator=(AVL<T>& orig)
{
	if (this != &orig)
	{
		//Limpio el receptor
		destruye(raiz);
		//Copio
		num = orig.num;
		Hoja<T> *original = orig.raiz;
		Hoja<T> *aux = new Hoja<T>(0, 0, orig.raiz->bal, orig.raiz->dato);
		raiz = aux;
		preordenC(original->izq, raiz->izq);
		preordenC(original->der, raiz->der);
	}
	return *this;
}

template<typename T>
bool AVL<T>::busca(T & dato, T & result)
{
	Hoja<T> *p = buscaClave(dato, raiz);
	if (p)
	{
		result = p->dato;
		return true;
	}
	else return false;
}

template<typename T>
bool AVL<T>::buscaIt(T & dato, T & result)
{
	bool esta = false;
	buscaItaux(raiz, dato, esta, result);
	return esta;
}

template <typename T>
void AVL<T>::recorreInorden()
{
	inorden(raiz, 0);
}

template<typename T>
unsigned int AVL<T>::numElementos()
{
	return num;
}

template<typename T>
unsigned int AVL<T>::altura()
{
	unsigned int alt = 0;
	inordenAlt(raiz, 0, alt);
	return alt;
}

template<typename T>
void AVL<T>::inordenAlt(Hoja<T>* p, unsigned int nivel, unsigned int& altura)
{
	if (p)
	{
		inordenAlt(p->izq, nivel + 1,altura);
		if (nivel > altura)
			altura = nivel;
		inordenAlt(p->der, nivel + 1,altura);
	}
}
