#pragma once
#include <list>
#include <vector>

template <typename T>
class Casilla
{
private:
	std::list<T> puntos;
public:
	template <class T> friend class MRegular;
	Casilla() :puntos() {}
	void insertar(const T& dato);
	T* buscar(const T& dato);
	bool borrar(const T& dato);
	std::list<T> getLista();
	void setLista(std::list<T>& p);
	int npuntos();
	~Casilla();
};

template <typename T>
class MallaRegular
{
private:
	float xMin, yMin, xMax, yMax;
	float tamaCasillaX, tamaCasillaY;

	std::vector<std::vector<Casilla<T>>> mr;

	Casilla<T>* obtenerCasilla(float x, float y);
public:
	MallaRegular(float aXMin, float aYMin, float aXMax, float aYMax, int nDivX, int nDivY);
	void reajustar(int nDivX, int nDivY);
	std::vector<T> buscarRango(float rxmin, float rymin, float rxmax, float rymax);
	void insertar(float x, float y, const T& dato);

	unsigned int maxElementosPorCelda();
	unsigned int maxElementosPorFila();
	unsigned int maxElementosporColumna();
	float promedioElementosPorCelda();
	~MallaRegular();
};


template<typename T>
void Casilla<T>::insertar(const T & dato)
{
	puntos.push_back(dato);
}

template<class T>
T * Casilla<T>::buscar(const T & dato)
{
	typename list <T>::iterator it;
	it = puntos.begin();
	for (; it != puntos.end(); ++it)
	{
		if (*it == dato)
			return &(*it);
	}
	return 0;
}

template<typename T>
bool Casilla<T>::borrar(const T & dato)
{
	typename list<T>::iterator it;
	it = puntos.begin();
	for (; it != puntos.end(); ++it)
	{
		if (*it == dato)
		{
			puntos.erase(it);
			return true;
		}
	}
	return false;
}

template<typename T>
int Casilla<T>::npuntos()
{
	return puntos.size();
}

template<typename T>
std::list<T> Casilla<T>::getLista()
{
	return puntos;
}

template<typename T>
void Casilla<T>::setLista(std::list<T>& p)
{
	puntos = p;
}

template <typename T>
Casilla<T>::~Casilla()
{
}

template<typename T>
Casilla<T>* MallaRegular<T>::obtenerCasilla(float x, float y)
{
	int i = (x - xMin) / tamaCasillaX;
	int j = (y - yMin) / tamaCasillaY;
	Casilla<T>* aux = nullptr;
	if (i < 0 || i > mr[0].size())
		return aux;
	if (j < 0 || j > mr.size())
		return aux;
	return &mr[j][i];
}

template<typename T>
MallaRegular<T>::MallaRegular(float aXMin, float aYMin, float aXMax, float aYMax, int nDivX, int nDivY) :
	xMin(aXMin),
	xMax(aXMax),
	yMin(aYMin),
	yMax(aYMax)
{
	tamaCasillaX = (xMax - xMin) / nDivX;
	tamaCasillaY = (yMax - yMin) / nDivY;
	mr.insert(mr.begin(), nDivY, std::vector<Casilla<T>>(nDivX));
}

template<typename T>
void MallaRegular<T>::reajustar(int nDivX, int nDivY)
{
	std::vector<std::vector<Casilla<T>>> aux;
	aux.insert(aux.begin(), nDivY, std::vector<Casilla<T>>(nDivX));
	std::list<T> backup;
	float NtCX = (xMax - xMin) / nDivX;
	float NtCY = (yMax - yMin) / nDivY;
	for (float i = 0; i < ((xMax - xMin) / tamaCasillaX); i++)
	{
		for (float j = 0; j < ((yMax - yMin) / tamaCasillaY); j++)
		{
			backup.splice(backup.end(), mr[j][i].getLista());
		}
	}
	tamaCasillaX = NtCX;
	tamaCasillaY = NtCY;
	mr = aux;
	typename std::list<T>::iterator ite = backup.begin();
	for (int i = 0; i < backup.size(); i++)
	{
		//insertar(ite->getX(), ite->getY(), *ite); //For NetBeans
		insertar(ite._Ptr->_Myval->getX(), ite._Ptr->_Myval->getY(), ite._Ptr->_Myval); //For Visual Studio
		ite++;
	}
}

template<typename T>
std::vector<T> MallaRegular<T>::buscarRango(float rxmin, float rymin, float rxmax, float rymax)
{
	std::vector<T> puntosR;
	int iMin = (rxmin - xMin) / tamaCasillaX;
	int jMin = (rymin - yMin) / tamaCasillaY;
	int iMax = (rxmax - xMin) / tamaCasillaX;
	int jMax = (rymax - yMin) / tamaCasillaY;
	for (iMin; iMin < iMax; iMin++)
	{
		for (jMin; jMin < jMax; jMin++)
		{
			std::list<T> aux = mr[jMin][iMin].getLista();
			typename std::list<T>::iterator ite = aux.begin();
			for (int i = 0; i < aux.size(); i++)
			{
				puntosR.push_back(*ite);
				ite++;
			}
		}
	}
	return puntosR;
}

template<typename T>
void MallaRegular<T>::insertar(float x, float y, const T & dato)
{
	Casilla<T>* c = obtenerCasilla(x, y);
	if (c != nullptr)
		c->insertar(dato);
}

template<class T>
unsigned int MallaRegular<T>::maxElementosPorCelda()
{
	unsigned int x = 0;
	for (int j = 0; j < (yMax - yMin) / tamaCasillaY; j++)
	{
		for (int i = 0; i < (xMax - xMin) / tamaCasillaX; i++)
		{
			if (mr[j][i].npuntos() > x)
				x = mr[j][i].npuntos();
		}
	}
	return x;
}

template<class T>
unsigned int MallaRegular<T>::maxElementosPorFila()
{
	unsigned int x = 0;
	for (int j = 0; j < (yMax - yMin) / tamaCasillaY; j++)
	{
		unsigned int fila = 0;
		for (int i = 0; i < (xMax - xMin) / tamaCasillaX; i++)
		{
			if (mr[j][i].npuntos() > x)
				fila += mr[j][i].npuntos();
		}
		if (fila > x)
			x = fila;
	}
	return x;
}

template<class T>
unsigned int MallaRegular<T>::maxElementosporColumna()
{
	unsigned int x = 0;
	for (int i = 0; i < (xMax - xMin) / tamaCasillaX; i++)
	{
		unsigned int columna = 0;
		for (int j = 0; j < (yMax - yMin) / tamaCasillaY; j++)
		{
			if (mr[j][i].npuntos() > x)
				columna += mr[j][i].npuntos();
		}
		if (columna > x)
			x = columna;
	}
	return x;
}

template<class T>
float MallaRegular<T>::promedioElementosPorCelda()
{
	float sum = 0.0f;
	for (int j = 0; j < (yMax - yMin) / tamaCasillaY; j++)
	{
		for (int i = 0; i < (xMax - xMin) / tamaCasillaX; i++)
		{
			sum += mr[j][i].npuntos();
		}
	}
	return sum / (tamaCasillaX * tamaCasillaX);
}

template <typename T>
MallaRegular<T>::~MallaRegular()
{
}