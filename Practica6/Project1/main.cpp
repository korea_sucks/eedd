#pragma once
#include <iostream>
#include <cmath>
#include <vector>
#include <map>
#include <list>
#include "Producto.h"
#include "Nodo.h"
#include "Iterador.h"
#include "PharmaDron.h"
#include "Cliente.h"
#include "Pedido.h"
#include "THashCerradaProducto.h"
#include "Dron.h"
#include "MallaRegular.h"

void visualiza(Cliente* ped)
{
	std::cout << "\nPedido: " << ped->getPedido()->getID() << "\nNombre: " << ped->getNombre() << "\nDNI: " << ped->getDNI() << "\nEstado: " << ped->getPedido()->getEstado() << "\n";
	std::cout << "======================================================\nCesta: \n";
	int t = ped->getPedido()->getCesta()->size();
	for (int i = 0; i < t; i++)
	{
		std::cout << ped->getPedido()->getCesta()->operator[](i)->getDesc() << ", " << ped->getPedido()->getCesta()->operator[](i)->getPvp() << " EUR\n";
	}
	std::cout << "======================================================\nTotal: " << ped->getPedido()->importe() << " EUR\n\n";
};

int main()
{
	// Inizializamos los elementos necesarios, PharmaDron, THash y Malla
	PharmaDron prueba(5425, 36.0, -9.5, 45.8, 5.5, 10, 20);
	prueba.cargaProductos("pharma3.csv");
	prueba.creaClientes("clientes_v2.csv");
	system("pause");

	// A�adimos Drones al PharmaDron
	Dron a(UTM(40.945533, -4.112324));
	Dron b(UTM(40.416925, -3.703516));
	Dron c(UTM(37.765121, -3.789917));
	prueba.aniadeDron(&a);
	prueba.aniadeDron(&b);
	prueba.aniadeDron(&c);

	// A�adimos algunos productos a los pedidos
	std::list<Producto*> produ = prueba.buscaProducto("crema");
	std::list<Producto*>::iterator ite = produ.begin();
	prueba.ingresaCliente("00359836D", "byaqxLjC")->addProducto(ite._Ptr->_Myval);
	ite++;
	prueba.ingresaCliente("00359836D", "byaqxLjC")->addProducto(ite._Ptr->_Myval);
	ite++;
	prueba.ingresaCliente("00184131C", "O7HhKHc")->addProducto(ite._Ptr->_Myval);
	ite++;
	prueba.ingresaCliente("00184131C", "O7HhKHc")->addProducto(ite._Ptr->_Myval);
	ite++;
	prueba.ingresaCliente("01905361U", "W8iwIqK")->addProducto(ite._Ptr->_Myval);
	ite++;
	prueba.ingresaCliente("01905361U", "W8iwIqK")->addProducto(ite._Ptr->_Myval);

	// Enviamos los pedidos y revisamos que se hayan enviado correctamente
	visualiza(prueba.ingresaCliente("00359836D", "byaqxLjC"));
	system("pause");
	Dron* aux = prueba.dronDisponible();
	std::cout << "\nSe han entregado " << prueba.llevarPedidosZona(aux,*aux->getPos(), 0.15f) << " Pedidos en Segovia\n\n" ;
	visualiza(prueba.ingresaCliente("00359836D", "byaqxLjC"));
	system("pause");
	std::cout << "Max Carga por filas: " << prueba.getCargaX() << "\nMax Carga por colunmas: " << prueba.getCargaY() << "\n";
	prueba.reajustarMalla(50,20);
	std::cout << "\nMax Carga por filas: " << prueba.getCargaX() << "\nMax Carga por colunmas: " << prueba.getCargaY() << "\n";
	system("pause");
	aux = prueba.dronDisponible();
	visualiza(prueba.ingresaCliente("00184131C", "O7HhKHc"));
	system("pause");
	std::cout << "\nSe han entregado " << prueba.llevarPedidosZona(aux, *aux->getPos(), 0.6f) << " Pedidos en Madrid\n\n";
	visualiza(prueba.ingresaCliente("00184131C", "O7HhKHc"));
	system("pause");
	std::cout << "Max Carga por filas: " << prueba.clientesCelda() << "\n";
	system("pause");
	aux = prueba.dronDisponible();
	visualiza(prueba.ingresaCliente("01905361U", "W8iwIqK"));
	system("pause");
	std::cout << "\nSe han entregado " << prueba.llevarPedidosZona(aux, *aux->getPos(), 0.35f) << " Pedidos en Jaen\n";
	visualiza(prueba.ingresaCliente("01905361U", "W8iwIqK"));
	system("pause");
	return 0;
}