#pragma once
#include <iostream>
#include <cmath>
#include <vector>
#include <map>
#include <list>
#include"Producto.h"
#include"Nodo.h"
#include"Iterador.h"
#include"PharmaDron.h"
#include"Cliente.h"
#include"Pedido.h"
#include "THashCerradaProducto.h"

void visualiza(Cliente& ped)
{
	std::cout << "\nPedido: " << ped.getPedido().getID() << "\nDirecion de entrega: " << ped.getDireccion() << "\nNombre: " << ped.getNombre() << "\nDNI: " << ped.getDNI() << "\n";
	std::cout << "======================================================\nCesta: \n";
	int t = ped.getPedido().cesta->size();
	for (int i = 0; i < t; i++)
	{
		std::cout << ped.getPedido().cesta->operator[](i).getDesc() << ", " << ped.getPedido().cesta->operator[](i).getPvp() << "EUR\n";
	}
	std::cout << "======================================================\nTotal: " << ped.getPedido().importe() << "EUR\n\n";
};

int main()
{
	//Entrenamiento tabla
	
	std::list<Producto> prueba;
	THashCerradaProducto pruebaCua1(5425,0);
	THashCerradaProducto pruebaDob1(5425, 1);
	THashCerradaProducto pruebaCua2(3500, 0);
	THashCerradaProducto pruebaDob2(3500, 1);
	THashCerradaProducto pruebaCua3(6425, 0);
	THashCerradaProducto pruebaDob3(6425, 1);
	std::string fileNameProductos = "pharma3.csv";
	std::set<std::string> terminos;
	std::cout << "Leyendo fichero \n";
	std::ifstream fichero;
	std::string linea;
	fichero.open(fileNameProductos.c_str());
	if (fichero.good()) {

		bool fin = false;
		while (fin == false) {
			Producto aux;
			getline(fichero, linea);
			if (fichero.eof()) {
				fin = true;
			}
			else {
				aux.fromCSV(linea);
				sacaterminos(aux.getDesc(), terminos);
				prueba.push_back(aux);
				std::list<Producto>::iterator ite = prueba.end();
				ite--;
				pruebaCua1.metedatos(&ite._Ptr->_Myval);
				pruebaDob1.metedatos(&ite._Ptr->_Myval);
				pruebaCua2.metedatos(&ite._Ptr->_Myval);
				pruebaDob2.metedatos(&ite._Ptr->_Myval);
				pruebaCua3.metedatos(&ite._Ptr->_Myval);
				pruebaDob3.metedatos(&ite._Ptr->_Myval);
			}

		}
		fichero.close();
		std::cout << "Fichero leido \n======================================================\n";
	}
	std::cout << "Terminos distintos: " <<terminos.size() << "\n";
	
	std::cout << "Resultados de la exploracion Cuadratica 1\n======================================================\n";
	std::cout << "Tama�o: " << pruebaCua1.tamaTabla() << "\n" << "Productos insertados: " << pruebaCua1.numProductos() << "\n";
	std::cout << "Factor de Carga: " << pruebaCua1.factorCarga() << "\n" << "Maximo Colisiones: " << pruebaCua1.maxColisiones() << "\n" << "Promedio de Colisiones: " << pruebaCua1.promedioColisiones() << "\n";

	std::cout << "\n======================================================\n";

	std::cout << "Resultados de la exploracion Doble 1\n======================================================\n";
	std::cout << "Tama�o: " << pruebaDob1.tamaTabla() << "\n" << "Productos insertados: " << pruebaDob1.numProductos() << "\n";
	std::cout << "Factor de Carga: " << pruebaDob1.factorCarga() << "\n" << "Maximo Colisiones: " << pruebaDob1.maxColisiones() << "\n" << "Promedio de Colisiones: " << pruebaDob1.promedioColisiones() << "\n";

	std::cout << "\n======================================================\n";

	system("pause");
	
	std::cout << "Resultados de la exploracion Cuadratica 2\n======================================================\n";
	std::cout << "Tama�o: " << pruebaCua2.tamaTabla() << "\n" << "Productos insertados: " << pruebaCua2.numProductos() << "\n";
	std::cout << "Factor de Carga: " << pruebaCua2.factorCarga() << "\n" << "Maximo Colisiones: " << pruebaCua2.maxColisiones() << "\n" << "Promedio de Colisiones: " << pruebaCua2.promedioColisiones() << "\n";

	std::cout << "\n======================================================\n";

	std::cout << "Resultados de la exploracion Doble 2\n======================================================\n";
	std::cout << "Tama�o: " << pruebaDob2.tamaTabla() << "\n" << "Productos insertados: " << pruebaDob2.numProductos() << "\n";
	std::cout << "Factor de Carga: " << pruebaDob2.factorCarga() << "\n" << "Maximo Colisiones: " << pruebaDob2.maxColisiones() << "\n" << "Promedio de Colisiones: " << pruebaDob2.promedioColisiones() << "\n";

	std::cout << "\n======================================================\n";

	system("pause");
	
	
	std::cout << "Resultados de la exploracion Cuadratica 3\n======================================================\n";
	std::cout << "Tama�o: " << pruebaCua3.tamaTabla() << "\n" << "Productos insertados: " << pruebaCua3.numProductos() << "\n";
	std::cout << "Factor de Carga: " << pruebaCua3.factorCarga() << "\n" << "Maximo Colisiones: " << pruebaCua3.maxColisiones() << "\n" << "Promedio de Colisiones: " << pruebaCua3.promedioColisiones() << "\n";

	std::cout << "\n======================================================\n";

	std::cout << "Resultados de la exploracion Doble 3\n======================================================\n";
	std::cout << "Tama�o: " << pruebaDob3.tamaTabla() << "\n" << "Productos insertados: " << pruebaDob3.numProductos() << "\n";
	std::cout << "Factor de Carga: " << pruebaDob3.factorCarga() << "\n" << "Maximo Colisiones: " << pruebaDob3.maxColisiones() << "\n" << "Promedio de Colisiones: " << pruebaDob3.promedioColisiones() << "\n";

	std::cout << "\n======================================================\n";
	
	system("pause");
	
	//Aplicacion en la pr�ctica

	std::cout << "Creando PharmaDron\n======================================================\n";
	PharmaDron test(5425);
	test.cargaProductos("pharma3.csv");
	test.creaClientes("clientes.csv");

	system("pause");
	std::cout << "\n======================================================\nAsignando Clientes objetivo\n======================================================\n";

	Cliente* u1 = test.ingresaCliente("00463415C", "TcyTS6ZOA");
	Cliente* u2 = test.ingresaCliente("98803670M", "pzTC1m9jsXvb");

	system("pause");
	std::cout << "\n======================================================\nAniadiendo Productos\n======================================================\n";

	std::vector<Producto*> c1 = test.buscaTermino("inyectable");
	std::vector<Producto*> c2 = test.buscaTermino("150mg");

	for (int i = 0; i < c1.size(); i++)
	{
		Producto* aux = c1[i];
		u1->addProducto(*aux);
	}
	visualiza(*u1);

	std::cout << "\n======================================================\n";
	system("pause");

	for (int i = 0; i < c2.size(); i++)
	{
		if(c2[i]->getPvp() < 50)
			u2->addProducto(*(c2[i]));
	}
	visualiza(*u2);

	std::cout << "\n======================================================\n";
	system("pause");

	return 0;
}