#include "Cliente.h"

bool Cliente::menorQ(int & pos, Cliente& cli)
{
	if (dni[pos] < cli.dni[pos])
		return true;
	if (dni[pos] > cli.dni[pos])
		return false;
	return false;
}

bool Cliente::mayorQ(int & pos,Cliente& cli)
{
	if (dni[pos] < cli.dni[pos])
		return false;
	if (dni[pos] > cli.dni[pos])
		return true;
	return false;
}

bool Cliente::igualQ(int & pos, Cliente & cli)
{
	if (dni[pos] == cli.dni[pos])
		return true;
	else return false;
}

Cliente::Cliente(const Cliente & orig):
	dni(orig.dni),
	nombre(orig.nombre),
	pass(orig.pass),
	posicion(orig.posicion),
	pedi(orig.pedi)
{
}

Cliente::Cliente(PharmaDron * pha):
	dni(""),
	nombre(""),
	pass(""),
	direccion(""),
	pharma(pha)
{
}

std::string Cliente::getDNI()
{
	return dni;
}

std::string Cliente::getNombre()
{
	return nombre;
}

std::string Cliente::getPass()
{
	return pass;
}

std::string Cliente::getDireccion()
{
	return direccion;
}

Pedido Cliente::getPedido()
{
	return pedi;
}

Cliente::Cliente(std::string d, std::string name, std::string pasw, std::string add) :
	dni(d), nombre(name), pass(pasw), direccion(add),pharma(0)
{
}

bool Cliente::operator==(Cliente & cli)
{
	if (dni == cli.dni)
		if (nombre == cli.nombre)
			if (pass == cli.pass)
				if (direccion == cli.direccion)
					if(posicion.getLatitud() == cli.posicion.getLatitud())
						if (posicion.getLongitud() == cli.posicion.getLongitud())
							return true;
	return false;
}

bool Cliente::operator<(Cliente & cli)
{
	int i = 0;
	while(i < 9)
	{
		if (menorQ(i, cli))
		{
			return true;
		}
		else if (igualQ(i, cli))
		{
			i++;
		}
		else if (mayorQ(i, cli))
			return false;
	}
	return false;
}

bool Cliente::operator>(Cliente & cli)
{
	int i = 0;
	while (i < 9)
	{
		if (mayorQ(i, cli))
		{
			return true;
		}
		else if (igualQ(i, cli))
		{
			i++;
		}
		else if (menorQ(i, cli))
			return false;
	}
	return false;
}

Cliente& Cliente::operator=(Cliente & orig)
{
	dni = orig.dni;
	nombre = orig.nombre;
	pass = orig.pass;
	direccion = orig.direccion;
	posicion = orig.posicion;
	pedi = orig.pedi;
	return *this;
}

void Cliente::fromCSV(std::string csv)
{
	float aux1 = 0;
	float aux2 = 0;
	std::stringstream ss(csv);
	std::locale prefs("");
	ss.imbue(prefs);

	std::getline(ss, dni, ';');
	std::getline(ss, pass, ';');
	std::getline(ss, nombre, ';');
	std::getline(ss, direccion, ';');
	ss >> aux1;
	posicion.setLatitud(aux1);
	ss.ignore();
	ss >> aux2;
	posicion.setLongitud(aux2);
}

std::list<Producto*> Cliente::buscarProducto(std::string subcadena)
{
	std::list<Producto*> busqueda;
	for (int i = 0; i < pedi.cesta->size(); i++)
	{
		if (pedi.cesta->operator[](i).getDesc().find(subcadena) != -1)
		{
			Producto* aux = &(pedi.cesta->operator[](i));
			busqueda.insert(busqueda.end(), aux);
		}
	}
	return busqueda;
}

void Cliente::addProducto(Producto p)
{
	pedi.nuevoProducto(p);
}

Cliente::~Cliente()
{
}

std::ostream& operator<<(std::ostream & os, const Cliente & obj)
{
	os << "DNI: " << obj.dni << " | " << "Nombre: " << obj.nombre << " | " << "Direccion: " << obj.direccion << " | " << obj.posicion << "\n";
	return os;
}
