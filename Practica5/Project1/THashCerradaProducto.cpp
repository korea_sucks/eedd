#include "THashCerradaProducto.h"



unsigned long djb2(unsigned char *str)
{
	unsigned long hash = 5381;
	int c;

	while (c = *str++)
		hash = ((hash << 5) + hash) + c;

	return hash;
}


void sacaterminos(std::string desc, std::set<std::string>& terminos)
{
	std::transform(desc.begin(), desc.end(), desc.begin(), ::tolower);
	std::stringstream stream;
	stream.str(desc);
	while (!stream.eof())
	{
		std::string aux;
		std::getline(stream, aux, ' ');
		if (aux.length() > 4)
		{
			terminos.insert(aux);
		}
	}
}

long int THashCerradaProducto::fdispcua(unsigned long clave, int tries, int size)
	//f(c) = (c +i^2)%tam
{
	return (clave + (int)std::pow(tries, 2)) % size;
}

//Calcula el primo anterior al tama�o
int THashCerradaProducto::fdispdob(const int tam)
{
	long int R = 0;
	bool *prime = new bool[tam];
	for (long int i = 2; i*i < tam; i++)
	{
		if (prime[i])
		{
			for (int j = i * i; j <= tam; j += i)
				prime[j] = false;
		}
	}
	for (int p = 2; p < tam; p++)
	{
		if (prime[p])
			R = p;
	}
	return R;
}

long int THashCerradaProducto::fdispdoble(unsigned long clave, int tries, int size)
	//f(c) = (c + i * h2(c))%tam
	//h2(c) = R - c % R 
{
	int r = fdispdob(size);
	return (clave + tries * (r - clave % r)) % size;
}

THashCerradaProducto::THashCerradaProducto(int tamTabla, bool dob):
	doble(dob),
	MaxCol(0),
	numCol(0),
	numProd(0)
{
	for (int i = 0; i < tamTabla; i++)
	{
		tabla.push_back(Entrada());
	}
}

bool THashCerradaProducto::insertar(long int aClave, std::string& term, Producto* dato)
{
		int tries = 0;
		bool insertado = false;
		if (!doble)
		{
			while (tries != 20 && !insertado)
			{
				int pos = fdispcua(aClave, tries, tabla.size());
				if (tabla[pos].getEstado() == false)
				{
					tabla[pos].setEstado(true);
					tabla[pos].setClave(aClave);
					tabla[pos].setTermino(term);
					tabla[pos].insertaDato(dato);
					insertado = true;
				}
				else
				{
					if (tabla[pos].getTermino() == term)
					{
						tabla[pos].insertaDato(dato);
						insertado = true;
					}
					else
					{
						tries++;
					}
				}
			}
			if (insertado)
				numProd++;
			if (tries > MaxCol)
				MaxCol = tries;
			numCol += tries;
			return insertado;
		}
		else
		{
			while (tries != 20 && !insertado)
			{
				int pos = fdispdoble(aClave, tries, tabla.size());
				if (tabla[pos].getEstado() == false)
				{
					tabla[pos].setEstado(true);
					tabla[pos].setClave(aClave);
					tabla[pos].setTermino(term);
					tabla[pos].insertaDato(dato);
					insertado = true;
				}
				else
				{
					if (tabla[pos].getTermino() == term)
					{
						tabla[pos].insertaDato(dato);
						insertado = true;
					}
					else
					{
						tries++;
					}
				}
			}
			if (insertado)
				numProd++;
			if (tries > MaxCol)
				MaxCol = tries;
			numCol += tries;
			return insertado;
		}
}

//Version avanzada insertar
void THashCerradaProducto::metedatos(Producto* dato)
{
	unsigned long int aClave = 0;
	std::set<std::string> terms;
	int terminosinsertados = 0;
	sacaterminos(dato->getDesc(),terms);
	std::set<std::string>::iterator ite = terms.begin();
	if (!doble)
	{
		for (int i = 0; i < terms.size(); i++)
		{
			int tries = 0;
			bool insertado = false;
			aClave = (unsigned long int)(djb2((unsigned char*)ite._Ptr->_Myval.c_str()));
			while (tries != 20 && !insertado)
			{
				int pos = fdispcua(aClave, tries, tabla.size());
				if (tabla[pos].getEstado() == false)
				{
					tabla[pos].setEstado(true);
					tabla[pos].setClave(aClave);
					tabla[pos].setTermino(ite._Ptr->_Myval);
					tabla[pos].insertaDato(dato);
					insertado = true;
					terminosinsertados++;
				}
				else
				{
					if (tabla[pos].getTermino() == ite._Ptr->_Myval)
					{
						tabla[pos].insertaDato(dato);
						insertado = true;
						terminosinsertados++;
					}
					else
					{
						tries++;
					}
				}
			}
			ite++;
			if (tries > MaxCol)
				MaxCol = tries;
			numCol += tries;
		}
		if (terminosinsertados == terms.size())
			numProd++;
	}
	else
	{
		for (int i = 0; i < terms.size(); i++)
		{
			int tries = 0;
			bool insertado = false;
			aClave = (unsigned long int)(djb2((unsigned char*)ite._Ptr->_Myval.c_str()));
			while (tries != 20 && !insertado)
			{
				int pos = fdispdoble(aClave, tries, tabla.size());
				if (tabla[pos].getEstado() == false)
				{
					tabla[pos].setEstado(true);
					tabla[pos].setClave(aClave);
					tabla[pos].setTermino(ite._Ptr->_Myval);
					tabla[pos].insertaDato(dato);
					insertado = true;
					terminosinsertados++;
				}
				else
				{
					if (tabla[pos].getTermino() == ite._Ptr->_Myval)
					{
						tabla[pos].insertaDato(dato);
						insertado = true;
						terminosinsertados++;
					}
					else
					{
						tries++;
					}
				}
			}
			ite++;
			if (tries > MaxCol)
				MaxCol = tries;
			numCol += tries;
			if (terminosinsertados == terms.size())
				numProd++;
		}
	}
}

std::vector<Producto*> THashCerradaProducto::buscar(unsigned long int clave, const std::string & term)
{
	int tries = 0;
	int pos = 0;
	if (!doble)
	{
		do {
			pos = fdispcua(clave, tries, tabla.size());
			tries++;
		} while (tabla[pos].getTermino() != term && tries != 20);
		if (tabla[pos].getTermino() != term && tries != 20)
			return tabla[pos].getDatos();
		else
			throw std::invalid_argument("Elemento no encontrado");
	}
	else
	{
		do {
			pos = fdispdoble(clave, tries, tabla.size());
			tries++;
		} while (tabla[pos].getTermino() != term);
		return tabla[pos].getDatos();
	}
}

unsigned int THashCerradaProducto::tamaTabla()
{
	return tabla.size();
}

unsigned int THashCerradaProducto::numProductos()
{
	return numProd;
}

unsigned int THashCerradaProducto::numTerminos()
{
	int terminos = 0;
	for (int i = 0; i < tabla.size(); i++)
	{
		if (tabla[i].getEstado() == true)
			terminos++;
	}
	return terminos;
}

unsigned int THashCerradaProducto::maxColisiones()
{
	return MaxCol;
}

float THashCerradaProducto::promedioColisiones()
{
	return numCol / numProd;
}

float THashCerradaProducto::factorCarga()
{
	return (float)numTerminos() / (float)tabla.size();
}

bool THashCerradaProducto::getDisTipe()
{
	return doble;
}

THashCerradaProducto::~THashCerradaProducto()
{
}

void Entrada::setClave(long int clav)
{
	clave = clav;
}

long int Entrada::getClave()
{
	return clave;
}

void Entrada::setEstado(bool est)
{
	estado = est;
}

bool Entrada::getEstado()
{
	return estado;
}

void Entrada::setTermino(std::string term)
{
	termino = term;
}

std::string Entrada::getTermino()
{
	return termino;
}

std::vector<Producto*> Entrada::getDatos()
{
	return datos;
}

void Entrada::insertaDato(Producto *dato)
{
	datos.push_back(dato);
}

Entrada::Entrada() :
	clave(0),
	termino(""),
	estado(false)
{
}

Entrada::Entrada(long int aClave, std::string aTermino, Producto * dato) :
	clave(aClave),
	termino(aTermino),
	estado(true)
{
	datos.push_back(dato);
}
