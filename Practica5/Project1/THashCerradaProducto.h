#pragma once
#include <vector>
#include <algorithm>
#include <string>
#include "Producto.h"
#include <cmath>
#include <set>

class Entrada
{
private:
	unsigned long int clave;
	std::string termino;
	bool estado;
	std::vector<Producto*> datos;
public:
	void setClave(long int clav);
	long int getClave();
	void setEstado(bool est);
	bool getEstado();
	void setTermino(std::string term);
	std::string getTermino();
	std::vector<Producto*> getDatos();
	void insertaDato(Producto* dato);
	Entrada();
	Entrada(long int aClave, std::string aTermino, Producto* dato);
};

class THashCerradaProducto
{
private:
	std::vector<Entrada> tabla;
	int numProd;
	int MaxCol;
	int numCol;
	bool doble;


	//Dispersion:
	long int fdispcua(unsigned long clave, int tries, int size);
	int fdispdob(const int size);
	long int fdispdoble(unsigned long clave, int tries, int size);
public:
	THashCerradaProducto(int tamTabla, bool dob = true);
	bool insertar(long int aClave, std::string& terms, Producto* dato);
	//Version avanzada insertar
	void metedatos(Producto* dato);
	std::vector<Producto*> buscar(unsigned long int clave, const std::string& term);
	unsigned int tamaTabla();
	unsigned int numProductos();
	unsigned int numTerminos();
	unsigned int maxColisiones();
	float promedioColisiones();
	float factorCarga();
	//funcion que devuelve true si es dispersion doble o false si es cuadratica
	bool getDisTipe(); 
	~THashCerradaProducto();
};

void sacaterminos(std::string desc, std::set<std::string>& terminos);

//Codificación
unsigned long djb2(unsigned char *str);
