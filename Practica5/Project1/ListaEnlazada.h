#pragma once

#include <iostream>
#include"Nodo.h"
#include"Iterador.h"

template <class T>
class ListaEnlazada
{
public:
	ListaEnlazada<T>();
	ListaEnlazada<T>(const ListaEnlazada<T>& origen);
	ListaEnlazada<T>& operator=(const ListaEnlazada<T>& origen);
	T& inicio();
	T& fin();
	Iterador<T> iterador();
	void insertaInicio(T& dato);
	void insertaFinal(T& dato);
	void inserta(Iterador<T> &i, T &dato);
	void borraInicio();
	void borraFinal();
	void borra(Iterador<T> &i);
	unsigned tam();
	ListaEnlazada<T> concatena(const ListaEnlazada &l);
	void borra(Iterador<T> &i,int numElementos);
	~ListaEnlazada();

private:
	Nodo<T> *cabecera;
	Nodo<T> *cola;
	int tama;
};

template <class T>
ListaEnlazada<T>::ListaEnlazada():
	cabecera(0),
	cola(0),
	tama(0)
{
}

template<class T>
ListaEnlazada<T>::ListaEnlazada(const ListaEnlazada<T>& origen):
	cabecera(0),
	cola(0),
	tama(origen.tama)
{
	Nodo<T> *aux1 = origen.cabecera;
	Nodo<T> *nuevo = new Nodo<T>(aux1->dato,0);
	cabecera = nuevo;
	Nodo<T> *aux2 = cabecera;
	if (origen.tama != 0)
	{
		while (aux1->sig != 0)
		{
			Nodo<T> *nuevol = new Nodo<T>(aux1->sig->dato, 0);
			aux2->sig = nuevol;
			aux2 = nuevol;
			aux1 = aux1->sig;
		}
		cola = aux2;
	}
}

template <class T>
ListaEnlazada<T>::~ListaEnlazada()
{
	while (cabecera != 0)
	{
		Nodo<T> *borrado = cabecera;
		cabecera = cabecera->sig;
		delete borrado;
	}
}

template<class T>
ListaEnlazada<T>& ListaEnlazada<T>::operator=(const ListaEnlazada<T>& origen)
{
	if (cabecera != origen.cabecera)
	{
		tama = origen.tama;
		//Limpio el receptor
		while (cabecera != 0)
		{
			Nodo<T> *borrado = cabecera;
			cabecera = cabecera->sig;
			delete borrado;
		}
		//Ahora copio la lista
		Nodo<T> *aux1 = origen.cabecera;
		Nodo<T> *nuevo = new Nodo<T>(aux1->dato,0);
		cabecera = nuevo;
		Nodo<T> *aux2 = cabecera;
		while (aux1->sig != 0)
		{
			Nodo<T> *nuevol = new Nodo<T>(aux1->sig->dato, 0);
			aux2->sig = nuevol;
			aux2 = nuevol;
			aux1 = aux1->sig;
		}
		cola = aux2;
		return *this;
	}
}

template<class T>
T& ListaEnlazada<T>::inicio()
{
	return *cabecera;
}

template<class T>
T & ListaEnlazada<T>::fin()
{
	return *cola;
}

template<class T>
Iterador<T> ListaEnlazada<T>::iterador()
{
	return Iterador<T>(cabecera);
}

template<class T>
void ListaEnlazada<T>::insertaInicio(T & dato)
{
	Nodo<T> *aux = new Nodo<T>(dato, cabecera);
	if (cabecera == 0 && cola == 0)
	{
		cabecera = aux;
		cola = aux;
	}
	else
	{
		cabecera = aux;
	}
	tama++;
}

template<class T>
void ListaEnlazada<T>::insertaFinal(T & dato)
{
	Nodo<T> *aux = new Nodo<T>(dato, 0);
	if (cabecera == 0 && cola == 0)
	{
		cabecera = aux;
		cola = aux;
	}
	else
	{
		cola->sig = aux;
		cola = aux;
	}
	tama++;
}

template<class T>
void ListaEnlazada<T>::inserta(Iterador<T> &i, T &dato)
{
	//Elemento esta en la lista
	Nodo<T> *este = cabecera;
	bool esta = false;
	while (!esta && este != nullptr)
	{
		if (este == i.current)
		{
			esta = true;
		}
		else
		{
			este = este->sig;
		}
	}
	if (esta)
	{
		if (tama != 1)
		{
			Nodo<T> *aux = cabecera;
			while (aux->sig != i.current) {
				aux = aux->sig;
			}
			Nodo<T> *nuevo = new Nodo<T>(dato, i.current);
			aux->sig = nuevo;
			tama++;
		}
		else
		{
			Nodo<T> *nuevo = new Nodo<T>(dato, i.current);
			cabecera = nuevo;
			tama++;
		}
	}
	else
	{
		throw std::invalid_argument("El elemento no se encuentra en esta lista");
	}
}

template<class T>
void ListaEnlazada<T>::borraInicio()
{
	if (cabecera != 0)
	{
		Nodo<T> *borrado = cabecera;
		if (cabecera == cola)
		{
			cabecera = 0;
			cola = 0;
		}
		else
		{
			cabecera = cabecera->sig;
		}
		delete borrado;
		tama--;
	}
	else
	{
		throw std::out_of_range("Intento de borrar lista vacia");
	}
}

template<class T>
void ListaEnlazada<T>::borraFinal()
{
	if (cabecera != 0)
	{
		Nodo<T> *borrado = cola;
		Nodo<T> *aux = cabecera;
		if (cabecera != cola)
		{
			while (aux->sig != cola)
			{
				aux = aux->sig;
			}
			cola = aux;
			cola->sig = 0;
			delete borrado;
			tama--;
		}
		else
		{
			delete borrado;
			cola = 0;
			cabecera = 0;
			tama--;
		}
	}
	else
	{
		throw std::out_of_range("Intento de borrar lista vacia");
	}
}

template<class T>
void ListaEnlazada<T>::borra(Iterador<T>& i)
{
	if (tama != 0)
	{
		Nodo<T> *borrar = i.current;
		Nodo<T> *pre = cabecera;
		if (pre != borrar)
		{
			while (pre->sig != borrar)
			{
				pre = pre->sig;
				if (pre == 0)
				{
					throw std::invalid_argument("El elemento a borrar no se encuentra en la lista");
				}
			}
			pre->sig = borrar->sig;
			delete borrar;
			tama--;
		}
		else
		{
			if (cola == cabecera)
			{
				if (borrar == pre)
				{
					cabecera = borrar->sig;
					cola = 0;
					delete borrar;
				}
				else
				{
					throw std::invalid_argument("El elemento a borrar no se encuentra en la lista");
				}
			}
			else
			{
				if (borrar == pre)
				{
					cabecera = borrar->sig;
					delete borrar;
				}
				else
				{
					throw std::invalid_argument("El elemento a borrar no se encuentra en la lista");
				}
			}
			tama--;
		}
	}
	else
	{
		throw std::out_of_range("Intento de borrar lista vacia");
	}
}

template<class T>
unsigned ListaEnlazada<T>::tam()
{
	return tama;
}

template<class T>
ListaEnlazada<T> ListaEnlazada<T>::concatena(const ListaEnlazada & l)
{
	ListaEnlazada<T> nueva;
	Nodo<T> *aux = cabecera;
	while (aux != 0)
	{
		nueva.insertaFinal(aux->dato);
		aux = aux->sig;
	}
	aux = l.cabecera;
	while (aux != 0)
	{
		nueva.insertaFinal(aux->dato);
		aux = aux->sig;
	}
	return nueva;
}

template<class T>
void ListaEnlazada<T>::borra(Iterador<T> &i, int numElementos)
{
	//Tama�o suficiente para borrar
	if (tama >= numElementos + 1)
	{
		//Elemento esta en la lista
		Nodo<T> *este = cabecera;
		bool esta = false;
		while (!esta && este != nullptr)
		{
			if (este == i.current)
			{
				esta = true;
			}
			else
			{
				este = este->sig;
			}
		}
		if (esta)
		{
			Nodo<T> *inicio = i.current;
			Nodo<T> *aux1 = i.current;
			Nodo<T> *aux2 = i.current;
			for (int i = 0; i <= numElementos; i++)
			{
				//no avanzo mas alla del final
				if (aux1 != nullptr)
				{
					aux1 = aux1->sig;
				}
				else
				{
					std::cerr <<"WARNING: Superado final de la lista, borrado hasta el final por defecto" << "\n";
					break;
				}
			}
			//ajusto para borrar los x elementos
			if (aux1 != nullptr)
			{
				aux1 = aux1->sig;
			}
			aux2 = inicio->sig;
			inicio->sig = aux1;
			//Compruebo si es el final para modificar cola
			if (inicio->sig == nullptr)
			{
				cola = inicio;
			}
			while (aux2 != aux1)
			{
				Nodo<T> *borrado = aux2;
				aux2 = aux2->sig;
				delete borrado;
				tama--;
			}

		}
		else
		{
			throw std::invalid_argument("El elemento a borrar no se encuentra en la lista");
		}
	}
	else
	{
		throw std::out_of_range("Tama�o insuficiente para el borrado especificado");
	}
}
