#include "PharmaDron.h"



PharmaDron::PharmaDron(int tamPharma):
	produ(THashCerradaProducto(tamPharma))
{
}

void PharmaDron::cargaProductos(std::string fileNameProductos)
{
	std::cout << "Leyendo fichero \n";
	std::ifstream fichero;
	std::string linea;
	fichero.open(fileNameProductos.c_str());
	if (fichero.good()) {

		bool fin = false;
		while (fin == false) {
			Producto aux;
			getline(fichero, linea);
			if (fichero.eof()) {
				fin = true;
			}
			else {
				aux.fromCSV(linea);
				productos.insert(productos.end(),aux);
				std::list<Producto>::iterator ite = productos.end();
				ite--;
				produ.metedatos(&ite._Ptr->_Myval);
			}

		}
		fichero.close();
		std::cout << "Fichero leido \n======================================================\n";
	}
	else
	{
		throw std::invalid_argument("El fichero no se puede abrir o est� danado");
	}
}

void PharmaDron::creaClientes(std::string fileNameClientes)
{
	std::cout << "Leyendo fichero \n";
	std::ifstream fichero;
	std::string linea;
	fichero.open(fileNameClientes.c_str());
	if (fichero.good()) {

		bool fin = false;
		while (fin == false) {
			Cliente aux(this);
			getline(fichero, linea);
			if (fichero.eof()) {
				fin = true;
			}
			else {
				aux.fromCSV(linea);
				clientes.insert(std::pair<std::string, Cliente>(aux.getDNI(),aux));
			}

		}
		fichero.close();
		std::cout << "Fichero leido \n======================================================\n";
	}
	else
	{
		throw std::invalid_argument("El fichero no se puede abrir o est� danado");
	}
}

void PharmaDron::aniadeProducto(Producto p)
{
	productos.insert(productos.end(),p);
}

void PharmaDron::nuevoCliente(Cliente c)
{
	clientes.insert(std::pair<std::string, Cliente>(c.getDNI() ,c));
}

void PharmaDron::borraCliente(std::string subcadena)
{
	std::map<std::string, Cliente>::iterator aux;
	aux = clientes.find(subcadena);
	if(aux._Ptr != 0)
		clientes.erase(aux);
}

std::list<Producto*> PharmaDron::buscaProducto(std::string subcadena)
{
	std::list<Producto*> busqueda;
	std::list<Producto>::iterator i = productos.begin();
	while (i != productos.end())
	{
		if (i._Ptr->_Myval.getDesc().find(subcadena) != -1)
		{
			Producto *aux = &i._Ptr->_Myval;
			busqueda.insert(busqueda.end(), aux);
		}
		i++;
	}
	return busqueda;
}

Cliente* PharmaDron::ingresaCliente(std::string dni, std::string pass)
{
	Cliente aux(dni);
	Cliente result;
	std::map<std::string, Cliente>::iterator it = clientes.find(dni);
	if (it != clientes.end())
	{
		if (pass == it._Ptr->_Myval.second.getPass())
			return &it._Ptr->_Myval.second;
		if (pass != it._Ptr->_Myval.second.getPass())
			throw std::invalid_argument("Contrase�a incorrecta");
	}
	else
		throw std::invalid_argument("El Cliente no esta registrado");
}


Pedido PharmaDron::verPedido(Cliente c)
{
	return c.getPedido();
}

std::vector<Producto*> PharmaDron::buscaTermino(std::string termino)
{
	return produ.buscar((djb2((unsigned char*)termino.c_str())), termino);
}

PharmaDron::~PharmaDron()
{
}
