#pragma once
#include "Producto.h"
#include"Cliente.h"
#include"Pedido.h"
#include "THashCerradaProducto.h"
#include <list>
#include <map>

class PharmaDron
{
private:
	THashCerradaProducto produ;
	std::list<Producto> productos;
	std::map<std::string, Cliente> clientes;
public:
	PharmaDron(int tamPharma);
	void cargaProductos(std::string fileNameProductos);
	void creaClientes(std::string fileNameClientes);
	void aniadeProducto(Producto p);
	void nuevoCliente(Cliente c);
	void borraCliente(std::string subcadena);
	std::list<Producto*> buscaProducto(std::string subcadena);
	Cliente* ingresaCliente(std::string dni, std::string pass);
	Pedido verPedido(Cliente c);
	std::vector<Producto*> buscaTermino(std::string termino);
	virtual ~PharmaDron();
};

