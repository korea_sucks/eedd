#pragma once
#include <iostream>
#include <cmath>

template <class T>
class VDinamico 
{
private:
	T *mem;
	long int tfisico;
	long int tlogico;
public:
	VDinamico<T>();
	VDinamico<T>(unsigned tam);
	VDinamico<T>(const VDinamico<T>& orig);
	VDinamico<T>(const VDinamico<T>& orig, unsigned inicio, unsigned num);
	VDinamico<T>& operator=(const VDinamico<T>& orig);
	T& operator[](unsigned pos);
	void insertar(const T& dato, unsigned pos = UINT_MAX);
	T borrar(unsigned pos = UINT_MAX);
	unsigned tam();
	~VDinamico();
};

template <class T>
VDinamico<T>::VDinamico()
{
	tlogico = 0;
	mem = new T[tfisico = 1];
}

template<class T>
VDinamico<T>::VDinamico(unsigned tam):
	tlogico(0)
{
	int a = pow(2, (int)log2(tam) + 1);
	mem = new T[tfisico = a];
}

template<class T>
VDinamico<T>::VDinamico(const VDinamico<T>& orig)
{
	mem = new T[tfisico = orig.tfisico];
	tlogico = orig.tlogico;
	for (int i = 0; i < tfisico; i++)
	{
		mem[i] = orig.mem[i];
	}
}

template<class T>
VDinamico<T>::VDinamico(const VDinamico<T>& orig, unsigned inicio, unsigned num)
{
	mem = new T[tfisico = orig.tfisico];
	tlogico = num;
	for (int i = 0; i < num; i++)
	{
		mem[i] = orig.mem[inicio + i];
	}
}

template<class T>
VDinamico<T>& VDinamico<T>::operator=(const VDinamico<T>& orig)
{
	if (mem != orig.mem) {
		delete[] mem;

		tfisico = orig.tfisico;
		tlogico = orig.tlogico;
		mem = new T[tfisico];
		for (int i = 0; i < tlogico; i++) {
			mem[i] = orig.mem[i];
		}
		return *this;
	}
}

template<class T>
T& VDinamico<T>::operator[](unsigned pos)
{
	if (pos > tlogico && pos > tfisico)
	{
		throw std::invalid_argument("El tama�o del vector es menor");
	}
	else
	{
		return mem[pos];
	}
}

template<class T>
void VDinamico<T>::insertar(const T & dato, unsigned pos)
{
	if (tfisico == tlogico) {
		T *vaux;
		vaux = new T[tfisico = tfisico * 2];
		for (int i = 0; i < tlogico; i++) {
			vaux[i] = mem[i];
		}
		delete[] mem;
		mem = vaux;
	}
	if (pos != UINT_MAX){
		for (int i = tlogico; i > pos; i--) {
			mem[i] = mem[i - 1];
		}
		mem[pos] = dato;
		tlogico++;
	}
	else {
		mem[tlogico] = dato;
		tlogico++;
	}
}

template<class T>
T VDinamico<T>::borrar(unsigned pos)
{
	T aux = mem[pos];	
	if (tlogico != 0) {
		if (pos == UINT_MAX) {
			tlogico--;
		}
		else {
			if (pos <= tlogico) {
				for (int i = pos; i < tlogico-1; i++) {
					mem[i] = mem[i + 1];
				}
				tlogico--;
			}
			else {
				throw std::invalid_argument("El vector es m�s pequenio");
			}
		}
	}
	else {
		throw std::invalid_argument("El vector est� vacio");
	}
	if (tlogico * 3 < tfisico) {

		tfisico = tfisico / 2;
		T *vaux = new T[tfisico];

		for (int i = 0; i < tlogico; i++) {
			vaux[i] = mem[i];
		}
		delete[] mem;
		mem = vaux;
	}
	return aux;
}

template<class T>
unsigned VDinamico<T>::tam()
{
	return tlogico;
}

template<class T>
VDinamico<T>::~VDinamico()
{
	delete[] mem;
}