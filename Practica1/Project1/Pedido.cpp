#include "Pedido.h"


Pedido::Pedido(Cliente & cli)
{
	id = cli.getDNI;
}

void Pedido::nuevoProducto(Producto* prod)
{
	Nodo<Producto>* nexto;
	cesta->getCabecera() = nexto;
	while (nexto != cesta->getCola())
	{
		nexto = nexto->siguiente;
	}
	nexto->siguiente = new Nodo<Producto>(prod, 0);
	cesta.iterador->setCola(nexto->siguiente);
}

float Pedido::importe()
{
	Nodo<Producto>* nexto;
	float suma = 0;
	cesta->getCabecera() = nexto;
	while (nexto != 0)
	{
		suma +=nexto->dato->pvp;
		nexto = nexto->dato->siguiente;
	}
	return suma;
}

Pedido::~Pedido()
{
}
