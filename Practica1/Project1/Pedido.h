#pragma once
#include "Cliente.h"
#include "Producto.h"

class Pedido
{
private:
	std::string id;
	std::string estado;
	//ListaEnlazada<Producto> cesta;
public:
	Pedido(Cliente& cli);
	void nuevoProducto(Producto* prod);
	float importe();
	~Pedido();
};

