#pragma once
#include<iostream>
#include<string>
#include<sstream>
#include<fstream>
#include "vdinamico.h"

class Producto
{
private:
	std::string codigo;
	std::string descripcion;
	float pvp;
public:
	Producto(std::string cod = "", std::string desc= "", float prec = 0.0f);
	~Producto();
	Producto& setCod(std::string cod);
	Producto& setDesc(std::string desc);
	Producto& setPvp(float prec);
	std::string getCod();
	std::string getDesc();
	float getPvp();
	void fromCSV(std::string cadena);
};

