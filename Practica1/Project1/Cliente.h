#pragma once
#include <iostream>
#include <string>

class Cliente
{
private:
	std::string dni;
	std::string nombre;
	std::string pass;
	std::string direccion;
public:
	std::string getDNI();
	Cliente(std::string d = "", std::string name = "", std::string pasw = "", std::string add = "");
	~Cliente();
};

