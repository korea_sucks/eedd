#include "Producto.h"


Producto::Producto(std::string cod, std::string desc, float prec) : codigo(cod), descripcion(desc), pvp(prec)
{

}

Producto::~Producto()
{
}

Producto & Producto::setCod(std::string cod)
{
	codigo = cod;
	return *this;
}

Producto & Producto::setDesc(std::string desc)
{
	descripcion = desc;
	return *this;
}

Producto & Producto::setPvp(float prec)
{
	pvp = prec;
	return *this;
}

std::string Producto::getCod()
{
	return codigo;
}

std::string Producto::getDesc()
{
	return descripcion;
}

float Producto::getPvp()
{
	return pvp;
}

void Producto::fromCSV(std::string cadena)
{
	std::stringstream ss(cadena);
	std::string aux;

	std::getline(ss, codigo, ';');
	std::getline(ss, descripcion, ';');
	ss >> pvp;
}