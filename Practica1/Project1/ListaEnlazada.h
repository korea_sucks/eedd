#pragma once

#include <iostream>

template <class T>
class ListaEnlazada
{
public:
	ListaEnlazada<T>();
	ListaEnlazada<T>(const ListaEnlazada<T>& origen);
	ListaEnlazada<T>& operator=(const ListaEnlazada<T>& origen);
	T& inicio();
	T& fin();
//	Iterador iterador();
	void insertaInicio(T& dato);
	void insertaFinal(T& dato);
	void borraInicio();
	void borraFinal();
//	void borra(Iterador &i);
	unsigned tam();
	ListaEnlazada<T> concatena(const ListaEnlazada &l);
//	void borra(Iterador &i, numElementos);
	~ListaEnlazada();

private:
	Nodo<T> *cabecera;
	Nodo<T> *cola;
	int tama;
};

template <class T>
ListaEnlazada<T>::ListaEnlazada():
	cabecera(0),
	cola(0),
	tama(0)
{
}

template<class T>
ListaEnlazada<T>::ListaEnlazada(const ListaEnlazada<T>& origen):
	cabecera(0),
	cola(0),
	tama(origen.tama)
{
	Nodo<T> *aux1 = origen.cabecera;
	Nodo<T> *nuevo = new Nodo<T>(aux1->dato, aux1->sig);
	cabecera = nuevo;
	Nodo<T> *aux2 = cabecera;
	while (aux1->sig != 0) 
	{
		Nodo<T> *nuevol = new Nodo<T>(aux1->sig->dato,0);
		aux2->sig = nuevol;
		aux2 = nuevol;
		aux1 = aux1->sig;
	}
	cola = aux2;
}

template <class T>
ListaEnlazada<T>::~ListaEnlazada()
{
	while (cabecera != 0)
	{
		Nodo<T> *borrado = cabecera;
		cabecera = cabecera->sig;
		delete borrado;
	}
}

template<class T>
ListaEnlazada<T>& ListaEnlazada<T>::operator=(const ListaEnlazada<T>& origen)
{
	if (cabecera != origen.cabecera)
	{
		tama = origen.tama;
		//Limpio el receptor
		while (cabecera != 0)
		{
			Nodo<T> *borrado = cabecera;
			cabecera = cabecera->sig;
			delete borrado;
		}
		//Ahora copio la lista
		Nodo<T> *aux1 = origen.cabecera;
		Nodo<T> *nuevo = new Nodo<T>(aux1->dato, aux1->sig);
		cabecera = nuevo;
		Nodo<T> *aux2 = cabecera;
		while (aux1->sig != 0)
		{
			Nodo<T> *nuevol = new Nodo<T>(aux1->sig->dato, 0);
			aux2->sig = nuevol;
			aux2 = nuevol;
			aux1 = aux1->sig;
		}
		cola = aux2;
	}else
	{
		//TODO excepciones
	}
	return this;
}

template<class T>
T& ListaEnlazada<T>::inicio()
{
	return *cabecera;
}

template<class T>
T & ListaEnlazada<T>::fin()
{
	return *cola;
}

template<class T>
void ListaEnlazada<T>::insertaInicio(T & dato)
{
	Nodo<T> aux = new Nodo<T>(dato, cabecera);
	cabecera = aux;
	tama++;
}

template<class T>
void ListaEnlazada<T>::insertaFinal(T & dato)
{
	Nodo<T> aux = new Nodo<T>(dato, 0);
	cola->sig = aux;
	cola = aux;
	tama++;
}

template<class T>
void ListaEnlazada<T>::borraInicio()
{
	if (cabecera != 0)
	{
		Nodo<T> *borrado = cabecera;
		cabecera = cabecera->sig;
		delete borrado;
		tama--;
	}
	else
	{
		//TODO excepciones
	}
}

template<class T>
void ListaEnlazada<T>::borraFinal()
{
	if (cabecera != 0)
	{
		Nodo<T> *borrado = cola;
		Nodo<T> *aux = cabecera;
		if (cabecera != cola)
		{
			while (aux->sig != cola)
			{
				aux = aux->sig;
			}
			cola = aux;
			delete borrado;
			tama--;
		}
		else
		{
			delete borrado;
			cola = 0;
			cabecera = 0;
			tama--;
		}
	}
	else
	{
		//TODO excepciones
	}
}
