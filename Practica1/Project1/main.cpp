#include <iostream>
#include <cmath>
#include"vdinamico.h"
#include"Producto.h"
#include"Nodo.h"
#include"ListaEnlazada.h"
#include"Cliente.h"
#include"Pedido.h"

template <class T>
void leerProducto(VDinamico<T>& v, std::string nomArchivo) {
	std::cout << "Leyendo fichero \n";
	std::ifstream fichero;
	std::string linea;
	fichero.open(nomArchivo.c_str());
	if (fichero.good()) {

		bool fin = false;
		while (fin == false) {
			T aux;
			getline(fichero, linea);
			if (fichero.eof()) {
				fin = true;
			}
			else {
				aux.fromCSV(linea);
				v.insertar(aux);
			}
			
		}
		fichero.close();
		std::cout << "Fichero leido \n";
	}
	else
	{
		throw std::invalid_argument("El fichero no se puede abrir o est� danado");
	}
}

int main()
{


	/*
	std::cout << "Instanciando vector" << std::endl;
	system("pause");
	//Instancio vector
	VDinamico<Producto> prueba;
	try
	{
		leerProducto(prueba, "pharma3.csv");
	}
	catch (std::invalid_argument &e)
	{
		std::cerr << e.what() << "\n";
	}
	for (int i = 0; i < prueba.tam(); i++) {
		std::cout << prueba.operator[](i).getCod() << ";" << prueba.operator[](i).getDesc() << ";" << prueba.operator[](i).getPvp() << "\n";
	}
	
	system("pause");
	std::cout << "Nuevo vector de cremas" << std::endl;
	//system("pause");

	// Nuevo vector de cremas
	VDinamico<Producto> cremas;
	std::string a = "crema";
	std::string b = "CREMA";
	try
	{
		for (int i = 0; i < prueba.tam(); i++) {
			if ((prueba[i].getDesc()).find(a) != std::string::npos || (prueba[i].getDesc()).find(b) != std::string::npos) {
				cremas.insertar(prueba[i]);
			}
		}
	}
	catch (std::invalid_argument &e)
	{
		std::cerr << e.what() << "\n";
	}
	for (int i = 0; i < cremas.tam(); i++) {
		std::cout << cremas[i].getCod() << ";" << cremas[i].getDesc() << ";" << cremas[i].getPvp() << "\n";
	}

	system("pause");
	std::cout << "Vector con precio entre 5 y 15" << std::endl;
	//system("pause");

	//Vector con precio entre 5 y 15
	VDinamico<Producto> prueba2(prueba);
	long int i = prueba2.tam();
	try
	{
		for (i; i >= 0; i--)
		{
			if (prueba2[i].getPvp() < 5 || prueba2[i].getPvp() > 15)
			{
				try
				{
					prueba2.borrar(i);
				}
				catch (std::invalid_argument &e)
				{
					std::cerr << e.what() << "\n";
				}
			}
		}
	}
	catch (std::invalid_argument &e)
	{
		std::cerr << e.what() << "\n";
	}
	for (int i = 0; i < prueba2.tam(); i++) {
		std::cout << prueba2[i].getCod() << ";" << prueba2[i].getDesc() << ";" << prueba2[i].getPvp() << "\n";
	}
	std::cout << "El programa ha terminado con exito \n";
	system("pause");
	*/
	return 0;
}