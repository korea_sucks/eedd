#pragma once

#include<iostream>

template<class T>
class Nodo
{
private:
	int tama;
public:
	T dato;
	Nodo *sig;

	Nodo<T>(T &aDato, Nodo *aSig = 0);
};

template<class T>
Nodo<T>::Nodo(T &aDato, Nodo * aSig):
	dato(aDato),
	sig(aSig)
{
}
