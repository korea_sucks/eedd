#pragma once
#include <iostream>
#include <string>
#include <list>
#include"Producto.h"
#include"UTM.h"
#include"Pedido.h"
#include<sstream>
#include<locale>

class PharmaDron;
class Cliente
{
private:
	std::string dni;
	std::string nombre;
	std::string pass;
	std::string direccion;
	UTM posicion;
	Pedido pedi;
	PharmaDron *pharma;

	bool menorQ(int& pos, Cliente& cli);
	bool mayorQ(int& pos,Cliente& cli);
	bool igualQ(int& pos, Cliente& cli);
public:
	Cliente(const Cliente& orig);
	Cliente(PharmaDron* pha);
	std::string getDNI();
	std::string getNombre();
	std::string getPass();
	std::string getDireccion();
	Pedido getPedido();
	Cliente( std::string d = "", std::string name = "", std::string pasw = "", std::string add = "");
	bool operator==(Cliente& cli);
	bool operator<(Cliente& cli);
	bool operator>(Cliente& cli);
	Cliente& operator=(Cliente& orig);
	friend std::ostream& operator<<(std::ostream& os,const Cliente& obj);
	void fromCSV(std::string csv);
	std::list<Producto*> buscarProducto(std::string subcadena);
	void addProducto(Producto p);
	~Cliente();
};

