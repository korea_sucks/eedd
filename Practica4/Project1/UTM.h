#pragma once
#include <iostream>

class UTM
{
private:
	float latitud;
	float longitud;
public:
	UTM(float la = 0, float lo = 0);
	UTM(const UTM& orig): latitud(orig.latitud), longitud(orig.longitud){}
	UTM& operator=(const UTM& orig) 
	{
		latitud = orig.latitud;
		longitud = orig.longitud;
		return *this;
	}
	void setLatitud(float a)
	{
		latitud = a;
	}
	void setLongitud(float a)
	{
		longitud = a;
	}
	float getLatitud()
	{
		return latitud;
	}
	float getLongitud()
	{
		return longitud;
	}
	friend std::ostream& operator<<(std::ostream& os, const UTM& obj)
	{
		os << "Latitud/Longitud: " << obj.latitud << "/" << obj.longitud;
		return os;
	}
	~UTM();
};

