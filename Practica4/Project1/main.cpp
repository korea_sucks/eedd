#pragma once
#include <iostream>
#include <cmath>
#include <vector>
#include <map>
#include <list>
#include"Producto.h"
#include"Nodo.h"
#include"Iterador.h"
#include"PharmaDron.h"
#include"Cliente.h"
#include"Pedido.h"

template <class T>
void leerProducto(VDinamico<T>& v, std::string nomArchivo) {
	std::cout << "Leyendo fichero \n";
	std::ifstream fichero;
	std::string linea;
	fichero.open(nomArchivo.c_str());
	if (fichero.good()) {

		bool fin = false;
		while (fin == false) {
			T aux;
			getline(fichero, linea);
			if (fichero.eof()) {
				fin = true;
			}
			else {
				aux.fromCSV(linea);
				v.insertar(aux);
			}
			
		}
		fichero.close();
		std::cout << "Fichero leido \n";
	}
	else
	{
		throw std::invalid_argument("El fichero no se puede abrir o est� danado");
	}
}

template <class T>
void leerProducto(ListaEnlazada<T>& l, std::string nomArchivo) {
	std::cout << "Leyendo fichero \n";
	std::ifstream fichero;
	std::string linea;
	fichero.open(nomArchivo.c_str());
	if (fichero.good()) {

		bool fin = false;
		while (fin == false) {
			T aux;
			getline(fichero, linea);
			if (fichero.eof()) {
				fin = true;
			}
			else {
				aux.fromCSV(linea);
				l.insertaFinal(aux);
			}

		}
		fichero.close();
		std::cout << "Fichero leido \n======================================================\n";
	}
	else
	{
		throw std::invalid_argument("El fichero no se puede abrir o est� danado");
	}
}


void visualiza(Cliente& ped)
{
	std::cout << "\nPedido: " << ped.getPedido().getID() << "\nDirecion de entrega: " << ped.getDireccion() << "\nNombre: " << ped.getNombre() << "\nDNI: " << ped.getDNI() << "\n";
	std::cout << "======================================================\nCesta: \n";
	int t = ped.getPedido().cesta->size();
	for (int i = 0; i < t; i++)
	{
		std::cout << ped.getPedido().cesta->operator[](i).getDesc() << ", " << ped.getPedido().cesta->operator[](i).getPvp() << "EUR\n";
	}
	std::cout << "======================================================\nTotal: " << ped.getPedido().importe() << "EUR\n\n";
};

int main()
{
	//Codigo Practica 3

	//Prueba 1
	/*
	std::map<int, int> arbol;
	for (int i = 0; i < 1000000; i++)
	{
		arbol.insert(i);
	}
	std::cout << "Altura del arbol: " << arbol.altura() << "\n";
	for (int i = 0; i < 2; i++)
	{
		int j = 0;
		int k = 0;
		std::cout << "Numero a buscar: ";
		std::cin >> j;
		bool esta = arbol.busca(j,k);
		if (esta)
			std::cout << "Busqueda Por Clave Encontrado\n";
		else
			std::cout << "Busqueda Por Clave No Encontrado\n";
		esta = arbol.buscaIt(j, k);
		if (esta)
			std::cout << "Busqueda Iterativa Encontrado\n";
		else
			std::cout << "Busqueda Iterativa No Encontrado\n";
	}
	system("pause");
	*/
	//Prueba 2
	
	try
	{
		PharmaDron pharma;
		pharma.cargaProductos("pharma3.csv");
		pharma.creaClientes("clientes.csv");
		system("pause");

		std::cout << "Iniciando busquedad de clientes(DNI)\n======================================================\n";

		std::string a = "10982609X";
		std::string ap = "pU7Pqqk";
		Cliente *a1;
		a1 = pharma.ingresaCliente(a, ap);

		std::string b = "92139205N";
		std::string bp = "ipbb9sHKa";
		Cliente *b2;
		b2 = pharma.ingresaCliente(b, bp);

		std::string c = "34536319T";
		std::string cp = "4pUF1OcsIG";
		Cliente *c3;
		c3 = pharma.ingresaCliente(c, cp);

		std::cout << "======================================================\nClientes Leidos" << "\n";
		system("pause");
		std::cout << "Preparando Pedidos\n";

		std::list<Producto*> al, bl, al2, bl2;
		al = pharma.buscaProducto("100ml");
		al2 = pharma.buscaProducto("100ML");
		al.splice(al.end(), al2);

		bl = pharma.buscaProducto("Crema");
		bl2 = pharma.buscaProducto("CREMA");
		bl.splice(bl.end(), bl2);

		std::cout << "======================================================\nPedidos Preparados" << "\n";
		system("pause");
		std::cout << "Cargando Pedidos\n";
		std::cout << "======================================================\nPrimer Cliente:\n";

		std::list<Producto*>::iterator it = al.begin();
		while (it != al.end())
		{
			Producto* aux = it._Ptr->_Myval;
			a1->addProducto(*aux);
			it++;
		}

		visualiza(*a1);
		std::cout << "\n======================================================\n";
		system("pause");

		it = bl.begin();
		while (it != bl.end())
		{
			Producto* aux = it._Ptr->_Myval;
			b2->addProducto(*aux);
			it++;
		}

		visualiza(*b2);
		std::cout << "\n======================================================\n";
		system("pause");

		it = al.begin();
		while (it != al.end())
		{
			Producto* aux = it._Ptr->_Myval;
			c3->addProducto(*aux);
			it++;
		}
		it = bl.begin();
		while (it != bl.end())
		{
			if (it._Ptr->_Myval->getDesc().find("100ml") == -1 && it._Ptr->_Myval->getDesc().find("100ML") == -1)
			{
				Producto* aux = it._Ptr->_Myval;
				c3->addProducto(*aux);
			}
			it++;
		}

		visualiza(*c3);
		std::cout << "\n======================================================\nPedidos Cargados\n";

		system("pause");

		//Codigo Practica 4

		std::cout << "======================================================\nAniadiendo nuevo Cliente\n";
		Cliente h4("77691779H","Alberto Rodriguez","smush69","C/Agustin Rodenas, 14");
		pharma.nuevoCliente(h4);
		std::cout << "======================================================\nCliente aniadido\n";

		system("pause");

		std::cout << "======================================================\nAsignando nuevos Productos\n";
		std::list<Producto*> l1, l2;
		l1 = pharma.buscaProducto("ALMAX");
		l2 = pharma.buscaProducto("ALGIDOL");

		it = l1.begin();
		int m1 = 500000;
		Producto* j1 = 0;
		while (it != l1.end())
		{
			if (m1 > it._Ptr->_Myval->getPvp())
			{
				m1 = it._Ptr->_Myval->getPvp();
				j1 = it._Ptr->_Myval;
			}
			it++;
		}

		it = l2.begin();
		int m2 = 500000;
		Producto* j2 = 0;
		while (it != l2.end())
		{
			if (m2 > it._Ptr->_Myval->getPvp())
			{
				m2 = it._Ptr->_Myval->getPvp();
				j2 = it._Ptr->_Myval;
			}
			it++;
		}

		std::string h = "77691779H";
		std::string hp = "smush69";
		Cliente *h5 = pharma.ingresaCliente(h, hp);
		if (!h5)
		{
			h5->addProducto(*j1);
			h5->addProducto(*j2);
			std::cout << "======================================================\nProductos asignados\n";
		}
		else
			std::cout << "======================================================\nProductos  no asignados\n";

		if (!h5)
			visualiza(*h5);
		else
			std::cout << "Cliente no encontrado\n";

		std::cout << "======================================================\nBorrando Cliente\n";
		pharma.borraCliente(h5->getDNI());
		std::cout << "======================================================\nComprobando borrado\n";
		try
		{
			Cliente *h6 = pharma.ingresaCliente(h, hp);
			std::cout << "Cliente encontrado\n";
		}
		catch (std::exception &e)
		{
			std::cout << e.what() << "\n";
		}
		std::cout << "======================================================\n";
		system("pause");

	}
	catch (const std::exception& e)
	{
		std::cerr << e.what() << "\n";
		system("pause");
	}
}